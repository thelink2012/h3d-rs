# Horde3D For Rust

This is still a work in progress.
Contributions are very welcome. Open a issue if you want to assign yourself to a item in the TODO list. Suggestions for the wrapping (including changing the current design) are welcome as well.

The goal is to be as minimalistic as possible *(as Horde3D is)* but being type-safe enough.

TODO _(By Priority)_
-----------------------------------------
+ More Integration Tests
+ Most of the functions categorized as *Basic functions* in the original documentation.
+ More `Res` methods
   + Wrapper for `h3dGetNextResource` by using iterators.
   + Wrapper for `h3dQueryUnloadedResource` by using iterators.
+ More `Node` Methods
   + Wrapper for `h3dFindNodes` and `h3dGetNodeFindResult` by using iterators.
   + Wrapper for `h3dCastRay` and `h3dGetCastRayResult` by using iterators.
+ Few other things marked as TODO in comments, mainly in `src/lib.rs`.
+ The Horde3D Utility Library
+ More Examples
   + Knight Demo
   + Chicago Demo
+ Improve Documentation

# Additional Note
Well this kinda of needs a rewrite, not very Rusty atm as I it was my first project ever in Rust I was still gasping the patterns.
