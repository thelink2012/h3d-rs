extern crate glfw;
extern crate h3d;
mod window;
use window::*;
use h3d::res::*;
use h3d::node::*;

fn main() {

    let mut window = SampleWindow::new();

    // Horde3D should be initialized after we have a context (i.e. a window to draw).
    let horde3d = h3d::init().unwrap();

    // Request and load requested resources from disk.
    let mut pipe_res = h3d::Res::new_pipeline("pipelines/forward.pipeline.xml", h3d::ResFlags::empty()).unwrap();
    let model_res = h3d::Res::new_scene_graph("models/man/man.scene.xml", h3d::ResFlags::empty()).unwrap();
    let anim_res = h3d::Res::new_animation("animations/man.anim", h3d::ResFlags::empty()).unwrap();
    // Load the resources specified above from the "examples/assets" directory.
    h3d::load_resources_from_disk(ASSETS_DIR).unwrap();

    // Add the man into the scene and apply a animation to it.
    let mut model = h3d::Node::new_nodes(h3d::Node::root(), &model_res.as_scene_graph());
    model.set_transform(&[0.0, -1.0, -5.0], &[0.0, 25.0, 0.0], &[1.0, 1.0, 1.0]);
    // Apply an animation to the man model in the slot 0.
    model.as_model_mut().setup_anim_stage(0, Some(&anim_res.as_animation()), 0, None, false);

    // Add a light into the scene and set it's radius of influence.
    let mut light = h3d::Node::new_light(h3d::Node::root(), "Light1", None, "LIGHTING", "SHADOWMAP");
    light.set_transform(&[0.0, 20.0, 0.0], &[0.0, 0.0, 0.0], &[1.0, 1.0, 1.0]);
    // Set radius of influence of the light.
    light.as_light_mut().set_radius(50.0);

    // Add a camera into the scene.
    let mut camera = h3d::Node::new_camera(h3d::Node::root(), "Camera", &pipe_res.as_pipeline());
    // Setup viewport and frustum by simulating a window resize event.
    app_events(&mut camera, glfw::WindowEvent::Size(APP_WIDTH as i32, APP_HEIGHT as i32));

    // Variable used to update the current frame of animation of the man.
    let mut anim_frame: f32 = 0.0;

    while !window.should_close() {
        window.handle_events(|e| app_events(&mut camera, e));

        // The animation should go in a frame-rate independent fashion, thus a delta time.
        // The man will perform 24 frames of the animation per second here.
        anim_frame = anim_frame + 24.0 * window.delta_time;
        // Update the current frame of animation to be used.
        model.as_model_mut().set_anim_params(0, anim_frame, 1.0);
        // Update the geometry and simulate the animation of the man.
        model.as_model_mut().update(UPDATE_ANIMATION | UPDATE_GEOMETRY);

        // Render the scene into the backbuffer.
        camera.as_camera_mut().render();
        // Tell the engine we are done with the current frame.
        h3d::finalize_frame();

        window.swap_buffers();
    }
}

fn app_events(cam_node: &mut h3d::Node, event: glfw::WindowEvent) {
    match event {
        glfw::WindowEvent::Size(w, h) => {
            // The window has been resized, setup the viewport and camera frustum
            // from the new specified width and height.
            let mut camera = cam_node.as_camera_mut();
            camera.set_viewport(0, 0, w as u32, h as u32);
            camera.setup_camera_view(45.0, (w as f32) / (h as f32), 0.1, 1000.0);
            camera.pipeline().as_pipeline_mut().resize_pipeline_buffers(w as usize, h as usize);
        },
        _ => (),
    }
}
