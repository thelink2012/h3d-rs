extern crate h3d;

fn main() {
    println!("Horde3D version: {}", h3d::get_version_string());
}
