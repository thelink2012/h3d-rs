// TODO replicate https://github.com/horde3d/Horde3D/blob/master/Horde3D/Samples/Knight/main.cpp behaviour
extern crate glfw;
use glfw::{Context};
use std::sync::mpsc::{Receiver};

// Configuration
pub static APP_WIDTH: u32 = 1024;
pub static APP_HEIGHT: u32 = 576;
pub static ASSETS_DIR: &'static str = "examples/assets";
//static FULLSCREEN: bool = false;
//static BENCHMARK_LENGTH: u32 = 600;

#[allow(dead_code)]
pub struct SampleWindow {
    glfw: glfw::Glfw,
    window: glfw::Window,
    events: Receiver<(f64, glfw::WindowEvent)>,
    fullscreen: bool,
    width: u32,
    height: u32,
    pub delta_time: f32,
    last_frame: f32,
}

impl SampleWindow {

    pub fn new() -> SampleWindow {
        let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

        let (mut window, events) = glfw.create_window(APP_WIDTH, APP_HEIGHT, "Horde3D Window", glfw::WindowMode::Windowed)
            .expect("Failed to create GLFW window.");

        window.set_size_polling(true);
        window.set_key_polling(true);
        window.make_current();
        glfw.set_swap_interval(0);

        SampleWindow {
            glfw: glfw,
            window: window,
            events: events,
            fullscreen: false,
            width: APP_WIDTH,
            height: APP_HEIGHT,
            delta_time: 0.0,
            last_frame: 0.0,
        }
    }

    pub fn should_close(&self) -> bool {
        return self.window.should_close();
    }

    pub fn handle_events<F>(&mut self, mut handler: F) where F: FnMut(glfw::WindowEvent) {
        self.glfw.poll_events();
        for tuple in glfw::flush_messages(&self.events) {
            handler(tuple.1);
        }
    }

    pub fn swap_buffers(&mut self) {
        self.window.swap_buffers();

        let new_frame: f32 = self.glfw.get_time() as f32;
        self.delta_time = new_frame - self.last_frame;
        self.last_frame = new_frame;
    }
}
