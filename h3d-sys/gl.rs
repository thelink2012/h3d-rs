
#[cfg(target_os = "windows")]
#[link(name = "opengl32")]
extern { }

#[cfg(target_os = "macos")]
#[link(name="OpenGL", kind="framework")]
extern { }

#[cfg(target_os = "linux")]
#[link(name="GL")]
extern { }
