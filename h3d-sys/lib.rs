#[allow(bad_style)]
#[allow(non_snake_case)]
extern crate libc;
use libc::{c_void, c_char, c_uchar, c_short, c_int, c_uint, c_float};
mod gl;

pub type H3DBool = i8;
pub type H3DNode = c_int;
pub type H3DRes  = c_int;

#[allow(non_upper_case_globals)]
pub const H3DRootNode: H3DNode = 1;

#[cfg(feature="Horde3DUtils")]
#[allow(non_upper_case_globals)]
pub const H3DUTMaxStatMode: c_int = 2;

#[repr(C)]
pub enum H3DOptions
{
    MaxLogLevel=1,
    MaxNumMessages,
    TrilinearFiltering,
    MaxAnisotropy,
    TexCompression,
    SRGBLinearization,
    LoadTextures,
    FastAnimation,
    ShadowMapSize,
    SampleCount,
    WireframeMode,
    DebugViewMode,
    DumpFailedShaders,
    GatherTimeStats
}

#[repr(C)]
pub enum H3DStats
{
    TriCount=100,
    BatchCount,
    LightPassCount,
    FrameTime,
    AnimationTime,
    GeoUpdateTime,
    ParticleSimTime,
    FwdLightsGPUTime,
    DefLightsGPUTime,
    ShadowsGPUTime,
    ParticleGPUTime,
    TextureVMem,
    GeometryVMem
}

#[repr(C)]
pub enum H3DResTypes
{
    Undefined=0,
    SceneGraph,
    Geometry,
    Animation,
    Material,
    Code,
    Shader,
    Texture,
    ParticleEffect,
    Pipeline
}

#[repr(C)]
pub enum H3DResFlags
{
    NoQuery=1,
    NoTexCompression=2,
    NoTexMipmaps=4,
    TexCubemap=8,
    TexDynamic=16,
    TexRenderable=32,
    TexSRGB=64
}

#[repr(C)]
pub enum H3DFormats
{
    Unknown=0,
    TEX_BGRA8,
    TEX_DXT1,
    TEX_DXT3,
    TEX_DXT5,
    TEX_RGBA16F,
    TEX_RGBA32F
}

#[repr(C)]
pub enum H3DGeoRes
{
    GeometryElem=200,
    GeoIndexCountI,
    GeoVertexCountI,
    GeoIndices16I,
    GeoIndexStream,
    GeoVertPosStream,
    GeoVertTanStream,
    GeoVertStaticStream
}

#[repr(C)]
pub enum H3DAnimRes
{
    EntityElem=300,
    EntFrameCountI
}

#[repr(C)]
pub enum H3DMatRes
{
    MaterialElem=400,
    SamplerElem,
    UniformElem,
    MatClassStr,
    MatLinkI,
    MatShaderI,
    SampNameStr,
    SampTexResI,
    UnifNameStr,
    UnifValueF4
}

#[repr(C)]
pub enum H3DShaderRes
{
    ContextElem=600,
    SamplerElem,
    UniformElem,
    ContNameStr,
    SampNameStr,
    SampDefTexResI,
    UnifNameStr,
    UnifSizeI,
    UnifDefValueF4
}

#[repr(C)]
pub enum H3DTexRes
{
    TextureElem=700,
    ImageElem,
    TexFormatI,
    TexSliceCountI,
    ImgWidthI,
    ImgHeightI,
    ImgPixelStream
}

#[repr(C)]
pub enum H3DPartEffRes
{
    ParticleElem=800,
    ChanMoveVelElem,
    ChanRotVelElem,
    ChanSizeElem,
    ChanColRElem,
    ChanColGElem,
    ChanColBElem,
    ChanColAElem,
    PartLifeMinF,
    PartLifeMaxF,
    ChanStartMinF,
    ChanStartMaxF,
    ChanEndRateF,
    ChanDragElem
}

#[repr(C)]
pub enum H3DPipeRes
{
    StageElem=900,
    StageNameStr,
    StageActivationI
}

#[repr(C)]
pub enum H3DNodeTypes
{
    Undefined=0,
    Group,
    Model,
    Mesh,
    Joint,
    Light,
    Camera,
    Emitter
}

#[repr(C)]
pub enum H3DNodeFlags
{
    NoDraw=1,
    NoCastShadow=2,
    NoRayQuery=4,
    Inactive=7
}

#[repr(C)]
pub enum H3DNodeParams
{
    NameStr=1,
    AttachmentStr
}

#[repr(C)]
pub enum H3DModel
{
    GeoResI=200,
    SWSkinningI,
    LodDist1F,
    LodDist2F,
    LodDist3F,
    LodDist4F,
    AnimCountI
}

#[repr(C)]
pub enum H3DMesh
{
    MatResI=300,
    BatchStartI,
    BatchCountI,
    VertRStartI,
    VertREndI,
    LodLevelI
}

#[repr(C)]
pub enum H3DJoint
{
    JointIndexI=400,
    _Dummy                  // https://github.com/rust-lang/rust/issues/10292
}

#[repr(C)]
pub enum H3DLight
{
    MatResI=500,
    RadiusF,
    FovF,
    ColorF3,
    ColorMultiplierF,
    ShadowMapCountI,
    ShadowSplitLambdaF,
    ShadowMapBiasF,
    LightingContextStr,
    ShadowContextStr
}

#[repr(C)]
pub enum H3DCamera
{
    PipeResI=600,
    OutTexResI,
    OutBufIndexI,
    LeftPlaneF,
    RightPlaneF,
    BottomPlaneF,
    TopPlaneF,
    NearPlaneF,
    FarPlaneF,
    ViewportXI,
    ViewportYI,
    ViewportWidthI,
    ViewportHeightI,
    OrthoI,
    OccCullingI
}

#[repr(C)]
pub enum H3DEmitter
{
    MatResI=700,
    PartEffResI,
    MaxCountI,
    RespawnCountI,
    DelayF,
    EmissionRateF,
    SpreadAngleF,
    ForceF3
}

#[repr(C)]
pub enum H3DModelUpdateFlags
{
    Animation=1,
    Geometry=2
}

#[link(name = "Horde3D")]
extern "C" {
    pub fn h3dGetVersionString() -> *const c_char;
    pub fn h3dCheckExtension(extensionName: *const c_char) -> H3DBool;
    pub fn h3dGetError() -> H3DBool;
    pub fn h3dInit() -> H3DBool;
    pub fn h3dRelease();
    pub fn h3dRender(cameraNode: H3DNode);
    pub fn h3dFinalizeFrame();
    pub fn h3dClear();
    pub fn h3dGetMessage(level: *mut c_int, time: *mut c_float) -> *const c_char;
    pub fn h3dGetOption(param: H3DOptions) -> c_float;
    pub fn h3dSetOption(param: H3DOptions, value: c_float) -> H3DBool;
    pub fn h3dGetStat(param: H3DStats, reset: H3DBool) -> c_float;
    pub fn h3dShowOverlays(verts: *const c_float, vertCount: c_int, colR: c_float, colG: c_float, colB: c_float, colA: c_float, materialRes: H3DRes, flags: c_int);
    pub fn h3dClearOverlays();
    pub fn h3dGetResType(res: H3DRes) -> c_int;
    pub fn h3dGetResName(res: H3DRes) -> *const c_char;
    pub fn h3dGetNextResource(typee: c_int, start: H3DRes) -> H3DRes;
    pub fn h3dFindResource(typee: c_int, name: *const c_char) -> H3DRes;
    pub fn h3dAddResource(typee: c_int, name: *const c_char, flags: c_int) -> H3DRes;
    pub fn h3dCloneResource(sourceRes: H3DRes, name: *const c_char) -> H3DRes;
    pub fn h3dRemoveResource(res: H3DRes) -> c_int;
    pub fn h3dIsResLoaded(res: H3DRes) -> H3DBool;
    pub fn h3dLoadResource(res: H3DRes, data: *const c_char, size: c_int) -> H3DBool;
    pub fn h3dUnloadResource(res: H3DRes);
    pub fn h3dGetResElemCount(res: H3DRes, elem: c_int) -> c_int;
    pub fn h3dFindResElem(res: H3DRes, elem: c_int, param: c_int, value: *const c_char) -> c_int;
    pub fn h3dGetResParamI(res: H3DRes, elem: c_int, elemIdx: c_int, param: c_int) -> c_int;
    pub fn h3dSetResParamI(res: H3DRes, elem: c_int, elemIdx: c_int, param: c_int, value: c_int);
    pub fn h3dGetResParamF(res: H3DRes, elem: c_int, elemIdx: c_int, param: c_int, compIdx: c_int) -> c_float;
    pub fn h3dSetResParamF(res: H3DRes, elem: c_int, elemIdx: c_int, param: c_int, compIdx: c_int, value: c_float);
    pub fn h3dGetResParamStr(res: H3DRes, elem: c_int, elemIdx: c_int, param: c_int) -> *const c_char;
    pub fn h3dSetResParamStr(res: H3DRes, elem: c_int, elemIdx: c_int, param: c_int, value: *const c_char);
    pub fn h3dMapResStream(res: H3DRes, elem: c_int, elemIdx: c_int, stream: c_int, read: H3DBool, write: H3DBool) -> *mut c_void;
    pub fn h3dUnmapResStream(res: H3DRes);
    pub fn h3dQueryUnloadedResource(index: c_int) -> H3DRes;
    pub fn h3dReleaseUnusedResources();
    pub fn h3dCreateTexture(name: *const c_char, width: c_int, height: c_int, fmt: c_int, flags: c_int) -> H3DRes;
    pub fn h3dSetShaderPreambles(vertPreamble: *const c_char, fragPreamble: *const c_char);
    pub fn h3dSetMaterialUniform(materialRes: H3DRes, name: *const c_char, a: c_float, b: c_float, c: c_float, d: c_float) -> H3DBool;
    pub fn h3dResizePipelineBuffers(pipeRes: H3DRes, width: c_int, height: c_int);
    pub fn h3dGetRenderTargetData(pipelineRes: H3DRes, targetName: *const c_char, bufIndex: c_int, width: *mut c_int, height: *mut c_int, compCount: *mut c_int, dataBuffer: *mut c_void, bufferSize: c_int) -> H3DBool;
    pub fn h3dGetNodeType(node: H3DNode) -> c_int;
    pub fn h3dGetNodeParent(node: H3DNode) -> H3DNode;
    pub fn h3dSetNodeParent(node: H3DNode, parent: H3DNode) -> H3DBool;
    pub fn h3dGetNodeChild(node: H3DNode, index: c_int) -> H3DNode;
    pub fn h3dAddNodes(parent: H3DNode, sceneGraphRes: H3DRes) -> H3DNode;
    pub fn h3dRemoveNode(node: H3DNode);
    pub fn h3dCheckNodeTransFlag(node: H3DNode, reset: H3DBool) -> H3DBool;
    pub fn h3dGetNodeTransform(node: H3DNode, tx: *mut c_float, ty: *mut c_float, tz: *mut c_float, rx: *mut c_float, ry: *mut c_float, rz: *mut c_float, sx: *mut c_float, sy: *mut c_float, sz: *mut c_float);
    pub fn h3dSetNodeTransform(node: H3DNode, tx: c_float, ty: c_float, tz: c_float, rx: c_float, ry: c_float, rz: c_float, sx: c_float, sy: c_float, sz: c_float);
    pub fn h3dGetNodeTransMats(node: H3DNode, relMat: *mut *const c_float, absMat: *mut *const c_float);
    pub fn h3dSetNodeTransMat(node: H3DNode, mat4x4: *const c_float);
    pub fn h3dGetNodeParamI(node: H3DNode, param: c_int) -> c_int;
    pub fn h3dSetNodeParamI(node: H3DNode, param: c_int, value: c_int);
    pub fn h3dGetNodeParamF(node: H3DNode, param: c_int, compIdx: c_int) -> c_float;
    pub fn h3dSetNodeParamF(node: H3DNode, param: c_int, compIdx: c_int, value: c_float);
    pub fn h3dGetNodeParamStr(node: H3DNode, param: c_int) -> *const c_char;
    pub fn h3dSetNodeParamStr(node: H3DNode, param: c_int, value: *const c_char);
    pub fn h3dGetNodeFlags(node: H3DNode) -> c_int;
    pub fn h3dSetNodeFlags(node: H3DNode, flags: c_int, recursive: H3DBool);
    pub fn h3dGetNodeAABB(node: H3DNode, minX: *mut c_float, minY: *mut c_float, minZ: *mut c_float, maxX: *mut c_float, maxY: *mut c_float, maxZ: *mut c_float);
    pub fn h3dFindNodes(startNode: H3DNode, name: *const c_char, typee: c_int) -> c_int;
    pub fn h3dGetNodeFindResult(index: c_int) -> H3DNode;
    pub fn h3dSetNodeUniforms(node: H3DNode, uniformData: *mut c_float, count: c_int);
    pub fn h3dCastRay(node: H3DNode, ox: c_float, oy: c_float, oz: c_float, dx: c_float, dy: c_float, dz: c_float, numNearest: c_int) -> c_int;
    pub fn h3dGetCastRayResult(index: c_int, node: H3DNode, distance: *mut c_float, intersection: *mut c_float) -> H3DBool;
    pub fn h3dCheckNodeVisibility(node: H3DNode, cameraNode: H3DNode, checkOcclusion: H3DBool, calcLod: H3DBool) -> c_int;
    pub fn h3dAddGroupNode(parent: H3DNode, name: *const c_char) -> H3DNode;
    pub fn h3dAddModelNode(parent: H3DNode, name: *const c_char, geometryRes: H3DRes) -> H3DNode;
    pub fn h3dSetupModelAnimStage(modelNode: H3DNode, stage: c_int, animationRes: H3DRes, layer: c_int, startNode: *const c_char, additive: H3DBool);
    pub fn h3dGetModelAnimParams(modelNode: H3DNode, stage: c_int, time: *mut c_float, weight: *mut c_float);
    pub fn h3dSetModelAnimParams(modelNode: H3DNode, stage: c_int, time: c_float, weight: c_float);
    pub fn h3dSetModelMorpher(modelNode: H3DNode, target: *const c_char, weight: c_float) -> H3DBool;
    pub fn h3dUpdateModel(modelNode: H3DNode, flags: c_int);
    pub fn h3dAddMeshNode(parent: H3DNode, name: *const c_char, materialRes: H3DRes, batchStart: c_int, batchCount: c_int, vertRStart: c_int, vertREnd: c_int) -> H3DNode;
    pub fn h3dAddJointNode(parent: H3DNode, name: *const c_char, jointIndex: c_int) -> H3DNode;
    pub fn h3dAddLightNode(parent: H3DNode, name: *const c_char, materialRes: H3DRes, lightingContext: *const c_char, shadowContext: *const c_char) -> H3DNode;
    pub fn h3dAddCameraNode(parent: H3DNode, name: *const c_char, pipelineRes: H3DRes) -> H3DNode;
    pub fn h3dSetupCameraView(cameraNode: H3DNode, fov: c_float, aspect: c_float, nearDist: c_float, farDist: c_float);
    pub fn h3dGetCameraProjMat(cameraNode: H3DNode, projMat: *mut c_float);
    pub fn h3dAddEmitterNode(parent: H3DNode, name: *const c_char, materialRes: H3DRes, particleEffectRes: H3DRes, maxParticleCount: c_int, respawnCount: c_int) -> H3DNode;
    pub fn h3dUpdateEmitter(emitterNode: H3DNode, timeDelta: c_float);
    pub fn h3dHasEmitterFinished(emitterNode: H3DNode) -> H3DBool;
}

#[cfg(feature="Horde3DUtils")]
#[link(name = "Horde3DUtils")]
#[allow(non_snake_case)]
extern "C" {
    pub fn h3dutFreeMem(ptr: *mut *mut c_char);
    pub fn h3dutDumpMessages() -> H3DBool;
    pub fn h3dutGetResourcePath(typee: c_int) -> *const c_char;
    pub fn h3dutSetResourcePath(typee: c_int, path: *const c_char);
    pub fn h3dutLoadResourcesFromDisk(contentDir: *const c_char) -> H3DBool;
    pub fn h3dutCreateGeometryRes(name: *const c_char, numVertices: c_int, numTriangleIndices: c_int, posData: *mut c_float, indexData: *mut c_uint, normalData: *mut c_short, tangentData: *mut c_short, bitangentData: *mut c_short, texData1: *mut c_float, texData2: *mut c_float) -> H3DRes;
    pub fn h3dutCreateTGAImage(pixels: *const c_uchar, width: c_int, height: c_int, bpp: c_int, outData: *mut *mut c_char, outSize: *mut c_int) -> H3DBool;
    pub fn h3dutScreenshot(filename: *const c_char) -> H3DBool;
    pub fn h3dutPickRay(cameraNode: H3DNode, nwx: c_float, nwy: c_float, ox: *mut c_float, oy: *mut c_float, oz: *mut c_float, dx: *mut c_float, dy: *mut c_float, dz: *mut c_float);
    pub fn h3dutPickNode(cameraNode: H3DNode, nwx: c_float, nwy: c_float) -> H3DNode;
    pub fn h3dutShowText(text: *const c_char, x: c_float, y: c_float, size: c_float, colR: c_float, colG: c_float, colB: c_float, fontMaterialRes: H3DRes);
    pub fn h3dutShowFrameStats(fontMaterialRes: H3DRes, panelMaterialRes: H3DRes, mode: c_int);
}
