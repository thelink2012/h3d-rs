use std::{mem};
use ffi;
use util::{get_error_flag, get_error_string, with_c_str, string_from_c_str, is_engine_alive};
use util::IS_H3D_ALIVE;

/// Result type for Horde3D functions that may fail.
pub type Result<T> = ::std::result::Result<T, String>;

/// The Horde3D engine instance. Initialise with `h3d::init()`.
///
/// Only one instance may be alive at once. The engine is not thread safe!
pub struct Engine;

/// Initialises the Horde3D engine.
///
/// This must be called before using any other Horde3D function.
///
/// This function initializes the graphics engine and makes it ready for use.
/// It has to be the first call to the engine except for `get_version_string`.
/// In order to successfully initialize the engine the calling application must provide a valid OpenGL context.
/// Unlike Horde's C API, the function cannot be called several times in different rendering contexts in order to initialize them, use `init_context` instead.
///
/// Wrapper for `h3dInit`.
///
/// # Example
///
/// ```no_run
/// fn run() {
///     let engine = h3d::init().unwrap();
///
///     /* Use Horde3D functionalities here */
///
/// } // Horde3D functionalities cannot be used after `engine` gets off scope.
/// ```
pub fn init() -> Result<Engine> {
    use std::sync::atomic::Ordering;

    let was_alive = IS_H3D_ALIVE.swap(true, Ordering::Relaxed);

    if was_alive {
        Err("Cannot have more than one `Engine` in use at the same time.".to_string())
    } else {
        match init_context() {
            Ok(_) => Ok(Engine),
            Err(e) => {
                IS_H3D_ALIVE.swap(false, Ordering::Relaxed);
                Err(e)
            }
        }
    }
}

/// Initialises the engine in another rendering context.
///
/// This function should not be called on the same rendering context as `init` was called but
/// instead in another context.
///
/// Wrapper for `h3dInit` in an additional rendering context.
pub fn init_context() -> Result<()> {
    unsafe_h3d!({
        if !is_engine_alive() {
            Err("Context cannot be initialized before a `Engine` instance is available.".to_string())
        } else {
            if ffi::h3dInit() != 0 {
                Ok(())
            } else {
                get_error_flag();   // consume the error flag if it was set
                Err(get_error_string())
            }
        }
    })
}

impl Drop for Engine {
    /// Wrapper for `h3dRelease`
    fn drop(&mut self) {
        use std::sync::atomic::Ordering;

        let was_alive = IS_H3D_ALIVE.swap(false, Ordering::Relaxed);
        assert!(was_alive);

        // Don't use unsafe_h3d macro here!
        unsafe { ffi::h3dRelease() }
    }
}

/// Returns the engine version string.
///
/// Wrapper for `h3dGetVersionString`.
pub fn get_version_string() -> String {
    // h3dGetVersionString can be called before the engine initializes.
    // That means we shouldn't use unsafe_h3d macro here!
    unsafe { string_from_c_str(ffi::h3dGetVersionString()) }
}


/// Gets the next message from the message queue.
///
/// This function returns the next message on the engine log queue, it's level of importance and
/// the time it was registered.
///
/// **Note:** The Rust binding consumes this log queue for `Result` reporting thus using
///  this function is not recommended since the queue is consumed.
///
/// Wrapper for `h3dGetMessage`.
pub fn get_message() -> Option<(String, i32, f32)> {
    unsafe { /* unsafe_h3d! not necessary */
        // TODO to fix the drawback of the binding consumption make a message callback thing!
        let (mut level, mut time) = (mem::uninitialized(), mem::uninitialized());
        let message = string_from_c_str(ffi::h3dGetMessage(&mut level, &mut time));
        if !message.is_empty() { Some((message, level, time)) } else { None  }
    }
}

// TODO
// h3dGetOption
// h3dSetOption
// h3dGetStat
// h3dShowOverlays
// h3dClearOverlays
// TODO
// h3dSetShaderPreambles
// h3dGetRenderTargetData

/// Removes all resources and scene nodes.
///
/// This function removes all nodes from the scene graph except the root node and releases all resources.
///
/// # Safety
///
/// All resource and node IDs are invalid after calling this function.
/// The user **must** make sure all nodes and resources are dropped off.
///
pub unsafe fn clear() {
    unsafe_h3d! { ffi::h3dClear() };
}

/// Marker for end of frame.
///
/// This function tells the engine that the current frame is finished and that all
/// subsequent rendering operations will be for the next frame.
///
/// Wrapper for `h3dFinalizeFrame`.
pub fn finalize_frame() {
    unsafe_h3d! { ffi::h3dFinalizeFrame() };
}

/// Frees resources that are no longer used (like a garbage collector).
///
/// This function releases resources that are no longer used.
/// Unused resources were either told to be released by dropping `Res` objects
/// or are no more referenced by any other engine objects.
///
/// Wrapper for `h3dReleaseUnusedResources`.
pub fn release_unused_resources() {
    unsafe_h3d! { ffi::h3dReleaseUnusedResources() }
}

/// Sets preambles of all Shader resources.
///
/// This function defines a header that is inserted at the beginning of all shaders.
/// The preamble is used when a shader is compiled, so changing it will not affect any shaders that
/// are already compiled.
///
/// The preamble is useful for setting platform-specific defines that can be employed for creating
/// several shader code paths, e.g. for supporting different hardware capabilities.
///
/// Wrapper for `h3dSetShaderPreambles`.
pub fn set_shader_preambles(vert_preamb: &str, frag_preamb: &str) {
    with_c_str(vert_preamb, |cvert| {
        with_c_str(frag_preamb, |cfrag| {
            unsafe_h3d! { ffi::h3dSetShaderPreambles(cvert, cfrag) };
        })
    })
}

/// Loads previously added resources from a data drive.
///
/// This utility function loads previously added and still unloaded resources from the specified
/// directories on a data drive.
///
/// Several search paths can be specified using the pipe character (`|`) as separator.
///
/// All resource names are directly converted to filenames and the function tries to find them in the
/// specified directories using the given order of the search paths.
///
/// Wrapper for `h3dutLoadResourcesFromDisk`.
#[cfg(feature="Horde3DUtils")]
pub fn load_resources_from_disk(content_dir: &str) -> Result<()> { // TODO path instead of str
    with_c_str(content_dir, |cdir| {
        unsafe_h3d! {
            if ffi::h3dutLoadResourcesFromDisk(cdir) == 0 {
                Err("Could not load one or more resources with h3dutLoadResourcesFromDisk.".to_string())
            } else {
                Ok(())
            }
        }
    })
}

/// Writes the content of the backbuffer to a tga file.
///
/// Wrapper for `h3dutScreenshot`.
#[cfg(feature="Horde3DUtils")]
pub fn screenshot(filename: &str) -> Result<()> {
    with_c_str(filename, |cfilename| {
        unsafe_h3d! {
            if ffi::h3dutScreenshot(cfilename) == 0 {
                Err("Could not perform h3dutScreenshot.".to_string())
            } else {
                Ok(())
            }
        }
    })
}

// TODO utils
// h3dutDumpMessages
// h3dutInitOpenGL
// h3dutReleaseOpenGL
// h3dutSwapBuffers
// h3dutGetResourcePath
// h3dutSetResourcePath
// h3dutCreateGeometryRes (put in Res impl)
// h3dutCreateTGAImage
// h3dutPickRay
// h3dutPickNode
// h3dutShowText
// h3dutShowFrameStats
