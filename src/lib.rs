#![crate_name = "h3d"]
#![allow(unused_unsafe)]    // TODO remove?
extern crate h3d_sys as ffi;
extern crate libc;
#[macro_use]
extern crate bitflags;

#[macro_use]
mod util;

#[allow(dead_code)] // TODO remove
pub mod h3d;
#[allow(dead_code)] // TODO remove
pub mod res;
#[allow(dead_code)] // TODO remove
pub mod node;
#[allow(dead_code)] // TODO remove


pub use util::get_error_flag; // TODO not pub...

pub use h3d::Engine;
pub use h3d::Result;

//pub use res::*;
pub use res::{Res, ResType, ResFlags, TexResFlags, ResWrapper};
pub use res::{UnsafeResStream, GeoResStream, TexResStream};
pub use res::{GeoIndexFormat, GeoIndexMapped, GeoIndexMappedMut, TexFormat, ParticleChannel};
pub use res::{GeoRes, GeoResMut};
pub use res::{TexRes, TexResMut};
pub use res::{AnimRes, AnimResMut};
pub use res::{MatRes, MatResMut};
pub use res::{CodeRes, CodeResMut};
pub use res::{ShaderRes, ShaderResMut};
pub use res::{PartEffRes, PartEffResMut};
pub use res::{PipeRes, PipeResMut};
pub use res::{SceneGraphRes, SceneGraphResMut};

//pub use node::*;
pub use node::{Node, NodeType, NodeFlags, NodeWrapper};
pub use node::{StereoIndex};
pub use node::{GroupNode, GroupNodeMut};
pub use node::{ModelNode, ModelNodeMut};
pub use node::{MeshNode, MeshNodeMut};
pub use node::{JointNode, JointNodeMut};
pub use node::{LightNode, LightNodeMut};
pub use node::{CameraNode, CameraNodeMut};
pub use node::{EmitterNode, EmitterNodeMut};

//pub use h3d::*;
pub use h3d::init;
pub use h3d::get_version_string;
pub use h3d::get_message;
pub use h3d::finalize_frame;
pub use h3d::release_unused_resources;
#[cfg(feature="Horde3DUtils")]
pub use h3d::load_resources_from_disk;
#[cfg(feature="Horde3DUtils")]
pub use h3d::screenshot;


// TODO pub use bitflag constants.

// TODO find out:
// * document that any func can panic for horde failures but that should not happen.
// * lots of 'as i32' should be 'as c_int' and lots of 'as i8' should be 'as ffi::H3DBool'
// * dynamic or static dispatch
// * is h3d::Node::root() ok?
// * is returning a heap allocated string fast enough? the C api returns a const char* ;/
// * should some of the places a tuple is returned (find for "-> (") use a new type / struct instead?
// * should it be node.as_camera_mut().render() or h3d::render(node.as_camera_mut()) ???
// * warning: unused variable: `horde3d`, #[warn(unused_variables)] on by default
// * usize or u32?
// * we have some aliasing problems, we can have multiple ref counted objects of type Res pointing to
//   the same resource. Is that something to get rid of?
// * why needs to use h3d::res::*; use h3d::node::*;
// * load_resources_from_disk modifies resources tho you may have a 'let resource' that mayn't be aware of that?

// TODO see the inconsitency between set_viewport and resize_pipeline_buffers size types

// TODO define proper #[derive] of enums and structs
