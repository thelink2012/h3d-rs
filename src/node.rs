use std::{mem, slice};
use libc::{c_int};
use ffi;
use util::{get_error_string, with_c_str, string_from_c_str};
use ::{Result, Res, GeoRes, TexResMut, AnimRes, MatRes, SceneGraphRes, PartEffRes, PipeRes};

/// Represents a node in the engine scene graph.
///
/// Wrapper for `H3DNode` type.
#[derive(Copy, Clone, Debug)]   // not owned :)
pub struct Node(ffi::H3DNode);

#[repr(i32)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
pub enum NodeType {
    Undefined = 0,  // XXX https://github.com/rust-lang/rust/issues/24469
    Group,
    Model,
    Mesh,
    Joint,
    Light,
    Camera,
    Emitter
}

bitflags! {
    flags NodeFlags: i32 {
        const NO_DRAW           = ffi::H3DNodeFlags::NoDraw as i32,
        const NO_CAST_SHADOW    = ffi::H3DNodeFlags::NoCastShadow as i32,
        const NO_RAY_QUERY      = ffi::H3DNodeFlags::NoRayQuery as i32,
        const INACTIVE          = ffi::H3DNodeFlags::Inactive as i32,
    }
}

// NOTE: Node does not own itself, the engine do! So no `Drop`ping.
impl Node {

    /// Gets the root node of the scene tree.
    #[inline]
    pub fn root() -> Node {
        unsafe { Node::from_raw(ffi::H3DRootNode) }
    }

    /// Adds a group node to the scene.
    ///
    /// This function creates a new group node and attaches it to the specified `parent` node.
    ///
    /// Wrapper for `h3dAddGroupNode`.
    pub fn new(parent: Node, name: &str) -> Node {
        with_c_str(name, |cname| {
            let cnode = unsafe_h3d! { ffi::h3dAddGroupNode(parent.raw(), cname) };
            Node::from_raw(cnode)
        })
    }

    /// Adds nodes from a scene graph resource to the scene.
    ///
    /// This function creates several new nodes as described in a scene graph resource and attaches them to a specified parent node.
    ///
    /// # Panics
    /// Panics if an invalid scene graph resource is specified or the scene graph resource is unloaded.
    ///
    /// Wrapper for `h3dAddNodes`.
    pub fn new_nodes(parent: Node, scene_graph: &SceneGraphRes) -> Node {
        let cnode =  unsafe_h3d! { ffi::h3dAddNodes(parent.raw(), scene_graph.as_res().raw()) };
        Node::from_raw(cnode)
    }

    /// Adds a model node to the scene.
    ///
    /// This function creates a new model node and attaches it to the specified `parent` node.
    ///
    /// Models nodes should be used to encapsulate mesh nodes.
    ///
    /// Wrapper for `h3dAddModelNode`.
    pub fn new_model(parent: Node, name: &str, geometry: &GeoRes) -> Node {
        with_c_str(name, |cname| {
            let cnode = unsafe_h3d! { ffi::h3dAddModelNode(parent.raw(), cname, geometry.as_res().raw()) };
            Node::from_raw(cnode)
        })
    }

    /// Adds a mesh node to the scene.
    ///
    /// This function creates a new mesh node and attaches it to the specified `parent` node.
    ///
    /// The `parent` or any subparent of a mesh node should be a model node where the geometry
    /// data will be taken from.
    /// The `batch_start` is the first triangle index in the geometry resource of a parent model node.
    /// The `batch_count` is the number of triangle indices used for drawing the mesh.
    /// The `vert_start` is the first vertex used in the geometry resource of a parent model node.
    /// The `vert_end` is the last vertex used in the geometry resource of a parent model node.
    ///
    /// Wrapper for `h3dAddMeshNode`.
    pub fn new_mesh(parent: Node, name: &str, material: &MatRes,
                    batch_start: u32, batch_count: u32,
                    vert_start: u32, vert_end: u32) -> Node {
        with_c_str(name, |cname| {
            let cnode = unsafe_h3d! {
                ffi::h3dAddMeshNode(parent.raw(), cname, material.as_res().raw(),
                                    batch_start as i32, batch_count as i32,
                                    vert_start as i32, vert_end as i32)
            };
            Node::from_raw(cnode)
        })
    }

    /// Adds a animation joint node to the scene.
    ///
    /// This function creates a new joint node and attaches it to the specified `parent` node.
    ///
    /// The `parent` or any subparent of a joint node should be a model node where the joint data will be taken from.
    ///
    /// The `joint_index` is the index of a joint in the geometry resource of a parent model node.
    ///
    /// Wrapper for `h3dAddJointNode`.
    pub fn new_joint(parent: Node, name: &str, joint_index: u32) -> Node {
        with_c_str(name, |cname| {
            let cnode = unsafe_h3d! {
                ffi::h3dAddJointNode(parent.raw(), cname, joint_index as i32)
            };
            Node::from_raw(cnode)
        })
    }

    /// Adds a light node to the scene.
    ///
    /// This function creates a new light node and attaches it to the specified `parent` node.
    ///
    /// The direction vector of the untransformed light node is pointing along the the negative z-axis.
    ///
    /// The specified `material` resource can define uniforms and projective textures.
    /// Furthermore it can contain a shader for doing lighting calculations if deferred shading is used.
    /// If no material is required the parameter can be `None`.
    ///
    /// The context names define which shader contexts are used when rendering shadow maps or
    /// doing light calculations for forward rendering configurations.
    ///
    /// Wrapper for `h3dAddLightNode`.
    pub fn new_light(parent: Node, name: &str, material: Option<&MatRes>,
                     lighting_context: &str, shadow_context: &str) -> Node {
        with_c_str(name, |cname| {
            with_c_str(lighting_context, |clighting| {
                with_c_str(shadow_context, |cshadow| {
                    let cnode = unsafe_h3d! {
                        ffi::h3dAddLightNode(parent.raw(), cname,
                                             material.map_or(0, |m| m.as_res().raw()),
                                             clighting, cshadow)
                    };
                    Node::from_raw(cnode)
                })
            })
        })
    }

    /// Adds a camera node to the scene.
    ///
    /// This function creates a new camera node and attaches it to the specified `parent` node.
    ///
    /// The `pipeline` is the pipeline resource used for redering.
    ///
    /// Wrapper for `h3dAddCameraNode`.
    pub fn new_camera(parent: Node, name: &str, pipeline: &PipeRes) -> Node {
        with_c_str(name, |cname| {
            let cnode = unsafe_h3d! {
                ffi::h3dAddCameraNode(parent.raw(), cname, pipeline.as_res().raw())
            };
            Node::from_raw(cnode)
        })
    }

    /// Adds a particle emmiter node to the scene.
    ///
    /// This function creates a new particle emmiter node and attaches it to the specified `parent` node.
    ///
    /// The `material` is the material used to render the particle.
    /// The `particle_fx` is the particle effect resource used for configuring particle properties.
    /// The `max_particles` is the maximum number of particles living at the same time.
    /// The `respawn_count` is the number of times a single particle is recreated after dying (or `None` for infinite).
    ///
    /// Wrapper for `h3dAddEmitterNode`.
    pub fn new_emmiter(parent: Node, name: &str, material: &MatRes, particle_fx: &PartEffRes,
                       max_particles: u32, respawn_count: Option<i32>) -> Node {
        with_c_str(name, |cname| {
            let cnode = unsafe_h3d! {
                ffi::h3dAddEmitterNode(parent.raw(), cname,
                                       material.as_res().raw(), particle_fx.as_res().raw(),
                                       max_particles as i32, respawn_count.unwrap_or(-1))
            };
            Node::from_raw(cnode)
        })
    }

    /// Constructs a `Node` from a raw `H3DNode`.
    ///
    /// # Safety
    ///
    /// Unline `Res::from_raw` this function is not unsafe since a `Node` object does not own the node.
    /// However this function is only safe to the point the Horde3D node handle is valid.
    ///
    #[inline]
    pub fn from_raw(cnode: ffi::H3DNode) -> Node {
        assert!(cnode != 0);
        Node(cnode)
    }

    /// Gets the contained `H3DNode` handle.
    #[inline]
    pub fn raw(&self) -> ffi::H3DNode {
        self.0
    }



    /// Returns the type of a scene node.
    ///
    /// Wrapper for `h3dGetNodeType`.
    pub fn kind(&self) -> NodeType {
        let result: NodeType = unsafe_h3d! { mem::transmute(ffi::h3dGetNodeType(self.raw())) };
        assert!(result != NodeType::Undefined);
        result
    }

    /// Gets the name of the scene node.
    ///
    /// Wrapper for `h3dGetNodeParamStr` with `H3DNodeParams::NameStr`.
    pub fn name(&self) -> String {
        unsafe_h3d! {
            h3dGetNodeParamStr(self.raw(), ffi::H3DNodeParams::NameStr as i32)
        }
    }

    /// Sets the name of the scene node.
    ///
    /// Wrapper for `h3dSetNodeParamStr` with `H3DNodeParams::NameStr`.
    pub fn set_name(&mut self, name: &str) {
        unsafe_h3d! {
            h3dSetNodeParamStr(self.raw(), ffi::H3DNodeParams::NameStr as i32, name)
        };
    }

    /// Gets the application-specific meta data for this node.
    ///
    /// This is an optional application-specific meta data for a node encapsulated in an ‘Attachment’ XML string.
    ///
    /// Wrapper for `h3dGetNodeParamStr` with `H3DNodeParams::AttachmentStr`.
    pub fn attachment_str(&self) -> String {
        unsafe_h3d! {
            h3dGetNodeParamStr(self.raw(), ffi::H3DNodeParams::AttachmentStr as i32)
        }
    }

    /// Sets the application-specific meta data for this node.
    ///
    /// See `attachment_str` for details.
    ///
    /// Wrapper for `h3dSetNodeParamStr` with `H3DNodeParams::AttachmentStr`.
    pub fn set_attachment_str(&mut self, s: &str) {
        unsafe_h3d! {
            h3dSetNodeParamStr(self.raw(), ffi::H3DNodeParams::AttachmentStr as i32, s)
        };
    }

    /// Returns the parent of a scene node.
    ///
    /// If there's no parent node or the parent node is the root node, `None` is returned.
    ///
    /// Wrapper for `h3dGetNodeParent`.
    pub fn parent(&self) -> Option<Node> {
        let cnode = unsafe_h3d! { ffi::h3dGetNodeParent(self.raw()) };
        if cnode == 0 { None } else { Some(Node::from_raw(cnode)) }
    }

    /// Relocates a node in the scene graph.
    ///
    /// This function relocates a scene node.
    /// It detaches the node from its current parent and attaches it to the specified new `parent` node.
    ///
    /// If the attachment to the new parent is not possible, the function returns `Err`.
    /// Relocation is not possible for the root node.
    ///
    /// Wrapper for `h3dSetNodeParent`.
    pub fn set_parent(&mut self, parent: Node) -> Result<()> {
        let is_okay = unsafe_h3d! { ffi::h3dSetNodeParent(self.raw(), parent.raw()) != 0 };
        if !is_okay { Err(get_error_string()) } else { Ok(()) }
    }

    /// Returns the handle to a child node.
    ///
    /// This function looks for the n-th (`index`) child node of a specified node and returns it.
    /// If the child doesn’t exist, the function returns `None`.
    ///
    /// Wrapper for `h3dGetNodeChild`.
    pub fn child(&self, index: u32) -> Option<Node> {
        let cnode = unsafe_h3d! { ffi::h3dGetNodeChild(self.raw(), index as c_int) };
        if cnode == 0 { None } else { Some(Node::from_raw(cnode)) }
    }

    /// Removes a node from the scene.
    ///
    /// This function removes the specified node and all of it’s children from the scene.
    /// Wrapper for `h3dRemoveNode`.
    pub fn remove(&mut self) {
        unsafe_h3d! { ffi::h3dRemoveNode(self.raw()) };
    }

    /// Checks if a scene node has been transformed by the engine.
    ///
    /// This function checks if a scene node has been transformed by the engine since the last time
    /// the transformation flag was reset. Therefore, it stores a flag that is set to true when a
    /// `Node::set_transform` function is called explicitely by the application or when the node
    /// transformation has been updated by the animation system.
    ///
    /// Immutable wrapper for `h3dCheckNodeTransFlag`.
    pub fn check_transform_flag(&self) -> bool {
        unsafe_h3d! { ffi::h3dCheckNodeTransFlag(self.raw(), 0) != 0 }
    }

    /// Resets the transform flag and returns the previous flag value.
    /// See `check_transform_flag` for details.
    ///
    /// Wrapper for `h3dCheckNodeTransFlag`.
    pub fn reset_transform_flag(&mut self) -> bool {
        unsafe_h3d! { ffi::h3dCheckNodeTransFlag(self.raw(), 1) != 0 }
    }


    /// Gets the relative transformation of a node.
    ///
    /// This function gets the `([translation], [rotation], [scale])` of a specified scene node object.
    /// The coordinates are in local space and contain the transformation of the node relative to its parent.
    ///
    /// Wrapper for `h3dGetNodeTransform`.
    pub fn transform(&self) -> ([f32; 3], [f32; 3], [f32; 3]) {
        unsafe_h3d!({
            let mut t: [f32; 3] = { mem::uninitialized() };
            let mut r: [f32; 3] = { mem::uninitialized() };
            let mut s: [f32; 3] = { mem::uninitialized() };
            ffi::h3dGetNodeTransform(self.raw(),
                                     &mut t[0], &mut t[1], &mut t[2],
                                     &mut r[0], &mut r[1], &mut r[2],
                                     &mut s[0], &mut s[1], &mut s[2]);
            (t, r, s)
        })
    }

    /// Sets the relative transformation of a node.
    ///
    /// This function sets the relative translation, rotation and scale of a specified scene node object.
    /// The coordinates are in local space and contain the transformation of the node relative to its parent.
    ///
    /// Wrapper for `h3dSetNodeTransform`.
    pub fn set_transform(&mut self, translation: &[f32; 3], rotation: &[f32; 3], scale: &[f32; 3]) {
        unsafe_h3d! {
            ffi::h3dSetNodeTransform(self.raw(),
                                     translation[0], translation[1], translation[2],
                                     rotation[0], rotation[1], rotation[2],
                                     scale[0], scale[1], scale[2])
        }
    }

    /// Returns the transformation matrices of a node.
    ///
    /// This function returns references of the `(relative, and_absolute)` matrices of the specified node.
    ///
    /// Wrapper for `h3dGetNodeTransMats`.
    pub fn transform_mat(&self) -> (&[f32], &[f32]) {
        unsafe_h3d!({
            let mut prel: *const f32 = { mem::uninitialized() };
            let mut pabs: *const f32 = { mem::uninitialized() };
            ffi::h3dGetNodeTransMats(self.raw(), &mut prel, &mut pabs);
            (slice::from_raw_parts(prel, 4*4), slice::from_raw_parts(pabs, 4*4))
        })
    }


    /// Sets the relative transformation matrix of a node.
    ///
    /// This function sets the relative transformation matrix of the specified scene node.
    /// It is basically the same as `set_transform` but takes directly a matrix instead of individual transformation parameters.
    ///
    /// TODO document matrix layout
    ///
    /// Wrapper for `h3dSetNodeTransMat`.
    ///
    /// # Panics
    ///
    /// Panics if `mat.len()` is not 16 (4x4 matrix).
    ///
    pub fn set_transform_mat(&mut self, mat: &[f32]) {
        assert_eq!(mat.len(), 16);
        unsafe_h3d! { ffi::h3dSetNodeTransMat(self.raw(), mat.as_ptr()) };
    }

    /// Gets the scene node flags.
    ///
    /// Wrapper for `h3dGetNodeFlags`.
    pub fn flags(&self) -> NodeFlags {
        unsafe_h3d! { NodeFlags::from_bits(ffi::h3dGetNodeFlags(self.raw())).unwrap() }
    }

    /// Sets the scene node flags.
    ///
    /// This function sets the scene node flags for the specified node.
    /// `recursive` specifies whether flags should be applied recursively to all child nodes.
    ///
    /// Wrapper for `h3dSetNodeFlags`.
    pub fn set_flags(&mut self, flags: NodeFlags, recursive: bool) {
        unsafe_h3d! { ffi::h3dSetNodeFlags(self.raw(), flags.bits(), recursive as i8) };
    }

    /// Gets the bounding box of a scene node.
    ///
    /// This function returns the world coordinates of the axis aligned bounding box of a specified node.
    /// The bounding box is represented using the `(minimum, and_maximum)` coordinates on all three axes.
    ///
    /// Wrapper for `h3dGetNodeAABB`.
    pub fn aabb(&self) -> ([f32; 3], [f32; 3]) {
        unsafe_h3d!({
            let mut min: [f32; 3] = { mem::uninitialized() };
            let mut max: [f32; 3] = { mem::uninitialized() };

            ffi::h3dGetNodeAABB(self.raw(),
                                &mut min[0], &mut min[1], &mut min[2],
                                &mut max[0], &mut max[1], &mut max[2]);
            (min, max)
        })
    }

    /// Checks the visibility of a node.
    ///
    /// This function checks if a specified node is visible from the perspective of a specified `camera`.
    /// The function always checks if the node is in the camera’s frustum.
    ///
    /// If `check_occl` is `true`, the function will take into account the occlusion culling information from the previous frame (if occlusion culling is disabled the flag is ignored).
    ///
    /// The flag `calc_lod` determines whether the detail level for the node should be returned in case it is visible.
    /// The function returns `None` if the node is not visible, otherwise `Some(0)` (base LOD level) or `Some(computed_lod_level)`.
    ///
    /// Wrapper for `h3dCheckNodeVisibility`.
    pub fn visibility(&self, camera: Node, check_occl: bool, calc_lod: bool) -> Option<i32> { // TODO enforce camera node
        unsafe_h3d!({
            let result = ffi::h3dCheckNodeVisibility(self.raw(), camera.raw(), check_occl as i8, calc_lod as i8);
            if result == -1 { None } else { Some(result) }
        })
    }

    // TODO h3dFindNodes h3dGetNodeFindResult
    // TODO h3dCastRay h3dGetCastRayResult

    /// Sets per-instance uniform data for a node.
    ///
    /// This function sets the custom per-instance uniform data for a node that can be accessed
    /// from within a shader.
    ///
    /// The uniform can be accessed within a shader using the `uniform vec4 customInstData[4]` uniform.
    /// The `v` array can have any number of elements up to 16.
    /// On each 4 indices of the `v` array another indice of the `customInstData` uniform will get used.
    ///
    /// Currently only model nodes will store this data.
    ///
    /// Wrapper for `h3dSetNodeUniforms`.
    pub fn set_uniforms(&mut self, v: &[f32]) {
        unsafe_h3d!({
            assert!(v.len() <= 16);
            ffi::h3dSetNodeUniforms(self.raw(), v.as_ptr() as *mut f32, v.len() as i32)
            // ^ Horde3D issues with const correctness forces us to do this cast
        });
    }

    /// Gets a reference to the group specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a group node.
    ///
    pub fn as_group(&self) -> GroupNodeData {
        assert_eq!(self.kind(), NodeType::Group);
        GroupNodeData { node: self }
    }

    /// Gets a reference to the mutable group specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a group node.
    ///
    pub fn as_group_mut(&mut self) -> GroupNodeDataMut {
        assert_eq!(self.kind(), NodeType::Group);
        GroupNodeDataMut { node: self }
    }

    /// Gets a reference to the model specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a model node.
    ///
    pub fn as_model(&self) -> ModelNodeData {
        assert_eq!(self.kind(), NodeType::Model);
        ModelNodeData { node: self }
    }

    /// Gets a reference to the mutable model specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a model node.
    ///
    pub fn as_model_mut(&mut self) -> ModelNodeDataMut {
        assert_eq!(self.kind(), NodeType::Model);
        ModelNodeDataMut { node: self }
    }

    /// Gets a reference to the mesh specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a mesh node.
    ///
    pub fn as_mesh(&self) -> MeshNodeData {
        assert_eq!(self.kind(), NodeType::Mesh);
        MeshNodeData { node: self }
    }

    /// Gets a reference to the mutable mesh specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a mesh node.
    ///
    pub fn as_mesh_mut(&mut self) -> MeshNodeDataMut {
        assert_eq!(self.kind(), NodeType::Mesh);
        MeshNodeDataMut { node: self }
    }

    /// Gets a reference to the joint specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a joint node.
    ///
    pub fn as_joint(&self) -> JointNodeData {
        assert_eq!(self.kind(), NodeType::Joint);
        JointNodeData { node: self }
    }

    /// Gets a reference to the mutable joint specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a joint node.
    ///
    pub fn as_joint_mut(&mut self) -> JointNodeDataMut {
        assert_eq!(self.kind(), NodeType::Joint);
        JointNodeDataMut { node: self }
    }

    /// Gets a reference to the light specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a light node.
    ///
    pub fn as_light(&self) -> LightNodeData {
        assert_eq!(self.kind(), NodeType::Light);
        LightNodeData { node: self }
    }

    /// Gets a reference to the mutable light specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a light node.
    ///
    pub fn as_light_mut(&mut self) -> LightNodeDataMut {
        assert_eq!(self.kind(), NodeType::Light);
        LightNodeDataMut { node: self }
    }

    /// Gets a reference to the camera specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a camera node.
    ///
    pub fn as_camera(&self) -> CameraNodeData {
        assert_eq!(self.kind(), NodeType::Camera);
        CameraNodeData { node: self }
    }

    /// Gets a reference to the mutable camera specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a camera node.
    ///
    pub fn as_camera_mut(&mut self) -> CameraNodeDataMut {
        assert_eq!(self.kind(), NodeType::Camera);
        CameraNodeDataMut { node: self }
    }

    /// Gets a reference to the emitter specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a emitter node.
    ///
    pub fn as_emitter(&self) -> EmitterNodeData {
        assert_eq!(self.kind(), NodeType::Emitter);
        EmitterNodeData { node: self }
    }

    /// Gets a reference to the mutable emitter specific data of this node.
    ///
    /// # Panics
    /// Panics if the resource is not a emitter node.
    ///
    pub fn as_emitter_mut(&mut self) -> EmitterNodeDataMut {
        assert_eq!(self.kind(), NodeType::Emitter);
        EmitterNodeDataMut { node: self }
    }
}

macro_rules! impl_node {
    ($WrapperName:ident, $WrapperNameMut:ident,
     $TraitName:ident, $TraitNameMut:ident,
     $ImplName:ident, $ImplBlock:item) => {

        /// Implements acessors and setters for node type specific data.
        ///
        $ImplBlock

        /// A accessor to node type specific data.
        pub struct $WrapperName<'a> {
            pub node: &'a Node,
        }

        /// A accessor and setter to node type specific data.
        pub struct $WrapperNameMut<'a> {
            pub node: &'a mut Node,
        }


        /// Trait implemented by the immutable and the mutable node accessors.
        pub trait $TraitName where Self: NodeWrapper, Self: $ImplName {
            // ...
        }

        /// Trait implemented by the mutable node accessor.
        pub trait $TraitNameMut where Self: $TraitName {
            // ...
        }

        // Implements `$TraitName` and `$TraitNameMut`.
        impl<'a> $TraitName for $WrapperName<'a> { }
        impl<'a> $TraitName for $WrapperNameMut<'a> { }
        impl<'a> $TraitNameMut for $WrapperNameMut<'a> { }

        // Implements `$ImplBlock`.
        impl<'a> $ImplName for $WrapperName<'a> { }
        impl<'a> $ImplName for $WrapperNameMut<'a> { }

        impl<'a> NodeWrapper for $WrapperName<'a> {
            /// Gets a reference to the wrapped node.
            fn as_node(&self) -> &Node {
                self.node
            }

            /// **Panics** since as this is a immutable wrapper.
            fn as_mut_node(&mut self) -> &mut Node {
                panic!("Immutable value trying to be mutable");
            }
        }

        impl<'a> NodeWrapper for $WrapperNameMut<'a> {
            /// Gets a reference to the wrapped node.
            fn as_node(&self) -> &Node {
                self.node
            }

            /// Gets a mutable reference to the wrapped node.
            fn as_mut_node(&mut self) -> &mut Node {
                self.node
            }
        }
    }
}

// TODO doc

#[allow(non_snake_case)]
unsafe fn h3dGetNodeParamFN(res: i32, param: i32, count: usize, arr: &mut [f32]) {
    debug_assert!(arr.len() == count);
    for x in 0..count {
        arr[x] = ffi::h3dGetNodeParamF(res, param, x as i32);
    }
}

#[allow(non_snake_case)]
unsafe fn h3dSetNodeParamFN(res: i32, param: i32, count: usize, arr: &[f32]) {
    debug_assert!(arr.len() == count);
    for x in 0..count {
        ffi::h3dSetNodeParamF(res, param, x as i32, arr[x]);
    }
}

#[allow(non_snake_case)]
unsafe fn h3dGetNodeParamStr(res: i32, param: i32) -> String {
    string_from_c_str(ffi::h3dGetNodeParamStr(res, param))
}

#[allow(non_snake_case)]
unsafe fn h3dSetNodeParamStr(res: i32, param: i32, value: &str) {
    with_c_str(value, |cvalue| ffi::h3dSetNodeParamStr(res, param, cvalue))
}



/// Wraps a node.
///
/// This is implemented by node specific accessors.
pub trait NodeWrapper {
    /// Gets a reference to the wrapped node.
    fn as_node(&self) -> &Node;
    /// Gets a mutable reference to the wrapped node or **panics** if the wrapper is not mutable.
    fn as_mut_node(&mut self) -> &mut Node;
}


/////////////////////////////
// Group-specific data
//

impl_node!(GroupNodeData, GroupNodeDataMut, GroupNode, GroupNodeMut, GroupNodeFns,

    pub trait GroupNodeFns : NodeWrapper {
    }
);


/////////////////////////////
// Model-specific data
//

bitflags! {
    flags ModelUpdateFlags: i32 {
        const UPDATE_ANIMATION  = ffi::H3DModelUpdateFlags::Animation as i32,
        const UPDATE_GEOMETRY   = ffi::H3DModelUpdateFlags::Geometry as i32,
    }
}

fn lodd_param(lod_level: i32) -> i32 {
    match lod_level {
        1 => ffi::H3DModel::LodDist1F as i32,
        2 => ffi::H3DModel::LodDist2F as i32,
        3 => ffi::H3DModel::LodDist3F as i32,
        4 => ffi::H3DModel::LodDist4F as i32,
        _ => unreachable!(),
    }
}

impl_node!(ModelNodeData, ModelNodeDataMut, ModelNode, ModelNodeMut, ModelNodeFns,

    pub trait ModelNodeFns : NodeWrapper {

        /// Gets the geometry resource used for the model.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DModel::GeoResI`.
        fn geometry(&self) -> Res {
            let handle = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DModel::GeoResI as i32)
            };
            Res::from_raw(handle)
        }

        /// Sets the geometry resource used for the model.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DModel::GeoResI`.
        fn set_geometry(&mut self, geometry: &GeoRes) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DModel::GeoResI as i32, geometry.as_res().raw())
            };
        }

        /// Gets whether software skinning is enabled.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DModel::SWSkinningI`.
        fn sw_skinning(&self) -> bool {
            unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DModel::SWSkinningI as i32) != 0
            }
        }

        /// Sets whether software skinning is enabled.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DModel::SWSkinningI`.
        fn set_sw_skinning(&mut self, is_software: bool) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DModel::SWSkinningI as i32, is_software as i32)
            };
        }

        /// Gets the distance to camera from which on LOD<N> is used.
        ///
        /// The `lod_level` must be between 1 and 4 (inclusive) otherwise it **panics**.
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DModel::LodDist<N>F`.
        fn lod_dist(&self, lod_level: i32) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), lodd_param(lod_level), 0)
            }
        }

        /// Sets the distance to camera from which on LOD<N> is used.
        ///
        /// The `lod_level` must be between 1 and 4 (inclusive) otherwise it **panics**.
        /// The `distance` must be positive, larger than 0.0 and greater than the previous lod level.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DModel::LodDist<N>F`.
        fn set_lod_dist(&mut self, lod_level: i32, distance: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), lodd_param(lod_level), 0, distance)
            }
        }

        /// Configures an animation stage of a model node.
        ///
        /// The function can be used for animation blending and mixing.
        ///
        /// There is a fixed number of stages (by default 16) on which different animations can be played.
        ///
        /// The `start_node` determines the first node (Joint or Mesh) to which the animation is recursively applied,
        /// if set to `None` the animation affects all animatable nodes (Joints and Meshes) of the model.
        ///
        /// If `None` is used for `anim_res`, the stage is cleared and the currently set animation is removed.
        ///
        /// The `layer` determines the priority of the animation and how the weights are distributed.
        /// See `set_anim_params` for more information.
        ///
        /// A simple way to do animation mixing is using additive animations.
        /// If a stage is configured to be `additive`, the engine calculates the difference between
        /// the current frame and the first frame in the animation and adds this delta, scaled by the
        /// weight factor, to the current transformation of the joints or meshes.
        /// Additive animations completely ignore the layer settings.
        /// They are only applied by the engine if a non-additive animation is assigned to the model as well.
        ///
        /// Wrapper for `h3dSetupModelAnimStage`.
        fn setup_anim_stage(&mut self, stage: i32, anim_res: Option<&AnimRes>, layer: i32,
                            start_node: Option<&str>, additive: bool) {
            with_c_str(start_node.unwrap_or(""), |cstart_node| {
                unsafe_h3d! {
                    ffi::h3dSetupModelAnimStage(self.as_mut_node().raw(), stage,
                                                anim_res.map_or(0, |a| a.as_res().raw()),
                                                layer, cstart_node, additive as i8)
                };
            })
        }

        /// Gets the animation stage parameters of a model node.
        ///
        /// This function gets the current animation `(time, and_weight)` for a specified `stage` of the
		/// specified model.
        ///
        /// The time corresponds to the frames of the animation and the animation is
		/// looped if the time is higher than the maximum number of frames in the animation resource.
        ///
		/// The weight is used for animation blending and determines how much influence the stage has compared
		/// to the other active stages. All weights of stages that are on the same layer are normalized.
        ///
        /// Wrapper for `h3dGetModelAnimParams`.
        fn anim_params(&self, stage: i32) -> (f32, f32) {
            unsafe_h3d!({
                let (mut time, mut weight) = (mem::uninitialized(), mem::uninitialized());
                ffi::h3dGetModelAnimParams(self.as_node().raw(), stage, &mut time, &mut weight);
                (time, weight)
            })
        }

        /// Sets the animation stage parameters of a model node.
        ///
        /// This function sets the current animation `time` and `weight` for a specified `stage` of the
        /// specified model.
        ///
        /// See `anim_params(...)` for details.
        ///
        /// Stages with a higher layer id are animated before stages with lower layers. The blend weight
        /// is distributed across the layers. If the weight sum of a layer is smaller than 1.0, the remaining
        /// weight is propagated to the next lower layer. So if a layer uses a weight of 100%, the lower layers
        /// will not get any weight and consequently not contribute to the overall animation.
        ///
        /// Wrapper for `h3dSetModelAnimParams`.
        fn set_anim_params(&mut self, stage: i32, time: f32, weight: f32) {
            unsafe_h3d! {
                ffi::h3dSetModelAnimParams(self.as_mut_node().raw(), stage, time, weight)
            };
        }

        /// Sets the weight of a morph target.
        ///
        /// If the `target` parameter is `None` the weight of all morph targets in the specified model node gets modified.
        /// If the specified morph target is not found the function returns `Err`.
        ///
        /// Wrapper for `h3dSetModelMorpher`.
        fn set_morpher(&mut self, target: Option<&str>, weight: f32) -> Result<()> {
            with_c_str(target.unwrap_or(""), |ctarget| {
                unsafe_h3d! {
                    if ffi::h3dSetModelMorpher(self.as_mut_node().raw(), ctarget, weight) == 0 {
                        Err(format!("Morph target '{}' not found during h3dSetMaterialUniform call.", target.unwrap()))
                    } else {
                        Ok(())
                    }
                }
            })
        }

        /// Applies animation and/or geometry updates.
        ///
        /// This function applies skeletal animation and geometry updates to the specified model, depending on
        /// the specified update flags. Geometry updates include morph targets and software skinning if enabled.
        /// If the animation or morpher parameters did not change, the function returns immediately.
        ///
        /// This function has to be called so that changed animation or morpher parameters will take effect.
        ///
        /// Wrapper for `h3dUpdateModel`.
        fn update(&mut self, flags: ModelUpdateFlags) {
            unsafe_h3d! {
                ffi::h3dUpdateModel(self.as_mut_node().raw(), flags.bits())
            }
        }
    }
);

/////////////////////////////
// Mesh-specific data
//

impl_node!(MeshNodeData, MeshNodeDataMut, MeshNode, MeshNodeMut, MeshNodeFns,

    pub trait MeshNodeFns : NodeWrapper {

        /// Gets the mesh lod level.
        ///
        /// The mesh is only rendered if its LOD level corresponds to the model’s current LOD level
        /// which is calculated based on the LOD distances.
        ///
        /// The returned value is between 0 and 4 (inclusive).
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DMesh::LodLevelI`.
        fn lod_level(&self) -> i32 {
            let lod_level = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DMesh::LodLevelI as i32)
            };
            assert!(lod_level >= 0 && lod_level <= 4);
            lod_level
        }

        /// Sets the mesh lod level.
        ///
        /// See `lod_level()` for details.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DMesh::LodLevelI`.
        fn set_lod_level(&mut self, lod_level: i32) {
            assert!(lod_level >= 0 && lod_level <= 4);
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DMesh::LodLevelI as i32, lod_level)
            };
        }

        /// Gets the material resource used for the mesh.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DMesh::MatResI`.
        fn material(&self) -> Res {
            let handle = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DMesh::MatResI as i32)
            };
            Res::from_raw(handle)
        }

        /// Sets the material resource used for the mesh.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DMesh::MatResI`.
        fn set_material(&mut self, material: &MatRes) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DMesh::MatResI as i32, material.as_res().raw())
            };
        }

        /// Gets the rendering batch information for this mesh.
        ///
        /// The returned tuple is `(the_first_triangle_index_of_mesh_in_the_geometry_resource,
        /// number_of_triangle_indices_used_for_drawing_mesh)`.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DMesh::BatchStartI` and `H3DMesh::BatchCountI`.
        fn batch(&self) -> (u32, u32) {
            unsafe_h3d! {(
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DMesh::BatchStartI as i32) as u32,
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DMesh::BatchCountI as i32) as u32,
            )}
        }

        /// Gets the rendering vertex information for this mesh.
        ///
        /// The returned tuple is `(the_first_vertex_index_of_mesh_in_the_geometry_resource,
        /// the_last_vertex_index_of_mesh_in_the_geometry_resource)`.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DMesh::VertRStartI` and `H3DMesh::VertREndI`.
        fn verts(&self) -> (u32, u32) {
            unsafe_h3d! {(
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DMesh::VertRStartI as i32) as u32,
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DMesh::VertREndI as i32) as u32,
            )}
        }
    }
);

/////////////////////////////
// Joint-specific data
//

impl_node!(JointNodeData, JointNodeDataMut, JointNode, JointNodeMut, JointNodeFns,

    pub trait JointNodeFns : NodeWrapper {

        /// Gets the index of the joint in the geometry resource of the parent model.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DJoint::JointIndexI`.
        fn index(&self) -> u32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DJoint::JointIndexI as i32) as u32
            }
        }
    }
);

/////////////////////////////
// Light-specific data
//

impl_node!(LightNodeData, LightNodeDataMut, LightNode, LightNodeMut, LightNodeFns,

    pub trait LightNodeFns : NodeWrapper {

        /// Gets the material resource used for the light.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DLight::MatResI`.
        fn material(&self) -> Res {
            let handle = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DLight::MatResI as i32)
            };
            Res::from_raw(handle)
        }

        /// Sets the material resource used for the light.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DLight::MatResI`.
        fn set_material(&mut self, material: &MatRes) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DLight::MatResI as i32, material.as_res().raw())
            };
        }

        /// Gets the radius of influence of the light (default is 100.0).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DLight::RadiusF`.
        fn radius(&self) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DLight::RadiusF as i32, 0)
            }
        }

        /// Sets the radius of influence of the light.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DLight::RadiusF`.
        fn set_radius(&mut self, radius: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DLight::RadiusF as i32, 0, radius)
            }
        }

        /// Gets the field of view angle of the light (default is 90.0).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DLight::FovF`.
        fn fov(&self) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DLight::FovF as i32, 0)
            }
        }

        /// Sets the field of view angle of the light.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DLight::FovF`.
        fn set_fov(&mut self, fov: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DLight::FovF as i32, 0, fov)
            }
        }

        /// Gets the diffuse color RGB of the light (default is [1.0, 1.0, 1.0]).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DLight::ColorF3`.
        fn color(&self) -> [f32; 3] {
            unsafe_h3d!({
                let mut rgb: [f32; 3] = { mem::uninitialized() };
                h3dGetNodeParamFN(self.as_node().raw(), ffi::H3DLight::ColorF3 as i32, 3, &mut rgb);
                rgb
            })
        }

        /// Sets the diffuse color RGB of the light.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DLight::ColorF3`.
        fn set_color(&mut self, rgb: &[f32; 3]) {
            unsafe_h3d!({
                h3dSetNodeParamFN(self.as_mut_node().raw(), ffi::H3DLight::ColorF3 as i32, 3, rgb);
            })
        }

        /// Gets diffuse color multiplier for altering intensity, mainly useful for HDR (default is 1.0).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DLight::ColorMultiplierF`.
        fn color_mut(&self) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DLight::ColorMultiplierF as i32, 0)
            }
        }

        /// Sets the diffuse color multiplier for altering intensity.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DLight::ColorMultiplierF`.
        fn set_color_mut(&mut self, mutp: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DLight::ColorMultiplierF as i32, 0, mutp)
            }
        }

        /// Gets the number of shadow maps used for light source.
        ///
        /// The value is either 0, 1, 2, 3 or 4. Defaults to 0.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DLight::ShadowMapCountI`.
        fn num_shadow_maps(&self) -> i32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DLight::ShadowMapCountI as i32)
            }
        }

        /// Sets the number of shadow maps used for light source.
        ///
        /// The `value` must be either 0, 1, 2, 3 or 4.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DLight::ShadowMapCountI`.
        fn set_num_shadow_maps(&mut self, value: i32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DLight::ShadowMapCountI as i32, value)
            }
        }

        /// Gets the bias value for shadow mapping to reduce shadow acne (default is 0.005).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DLight::ShadowMapBiasF`.
        fn shadowmap_bias(&self) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DLight::ShadowMapBiasF as i32, 0)
            }
        }

        /// Sets the bias value for shadow mapping to reduce shadow acne.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DLight::ShadowMapBiasF`.
        fn set_shadowmap_bias(&mut self, bias: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DLight::ShadowMapBiasF as i32, 0, bias)
            }
        }

        /// Gets the name of shader context used for computing lighting.
        ///
        /// Wrapper for `h3dGetNodeParamStr` with `H3DLight::LightingContextStr`.
        fn lighting_context(&self) -> String {
            unsafe_h3d! {
                h3dGetNodeParamStr(self.as_node().raw(), ffi::H3DLight::LightingContextStr as i32)
            }
        }

        /// Sets the name of shader context used for computing lighting.
        ///
        /// Wrapper for `h3dSetNodeParamStr` with `H3DLight::LightingContextStr`.
        fn set_lighting_context(&mut self, context: &str) {
            unsafe_h3d! {
                h3dSetNodeParamStr(self.as_mut_node().raw(), ffi::H3DLight::LightingContextStr as i32, context)
            }
        }

        /// Gets the name of shader context used for generating shadow maps.
        ///
        /// Wrapper for `h3dGetNodeParamStr` with `H3DLight::ShadowContextStr`.
        fn shadow_context(&self) -> String {
            unsafe_h3d! {
                h3dGetNodeParamStr(self.as_node().raw(), ffi::H3DLight::ShadowContextStr as i32)
            }
        }

        /// Sets the name of shader context used for generating shadow maps.
        ///
        /// Wrapper for `h3dSetNodeParamStr` with `H3DLight::ShadowContextStr`.
        fn set_shadow_context(&mut self, context: &str) {
            unsafe_h3d! {
                h3dSetNodeParamStr(self.as_mut_node().raw(), ffi::H3DLight::ShadowContextStr as i32, context)
            }
        }

    }
);

/////////////////////////////
// Camera-specific data
//

#[repr(i32)]
pub enum StereoIndex {
    LeftEye  = 0,
    RightEye = 1,
}

impl_node!(CameraNodeData, CameraNodeDataMut, CameraNode, CameraNodeMut, CameraNodeFns,

    pub trait CameraNodeFns : NodeWrapper {

        /// Gets the pipeline resource used for rendering with the camera.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DCamera::PipeResI`.
        fn pipeline(&self) -> Res {
            let handle = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::PipeResI as i32)
            };
            Res::from_raw(handle)
        }

        /// Sets the pipeline resource used for rendering with the camera.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DCamera::PipeResI`.
        fn set_pipeline(&mut self, pipeline: &PipeRes) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::PipeResI as i32, pipeline.as_res().raw())
            };
        }

        /// Gets the 2D texture resource used as output buffer or `None` for the main framebuffer (default is `None`).
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DCamera::OutTexResI`.
        fn render_texture(&self) -> Option<Res> {
            let handle = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::OutTexResI as i32)
            };
            if handle == 0 { None } else { Some(Res::from_raw(handle)) }
        }

        /// Sets the 2D texture resource used as output buffer or `None` for the main framebuffer.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DCamera::OutTexResI`.
        fn set_render_texture(&mut self, tex: Option<&mut TexResMut>) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::OutTexResI as i32, tex.map_or(0, |t| t.as_mut_res().raw()))
            };
        }

        /// Gets the index of the output buffer for stereoscopy rendering.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DCamera::OutBufIndexI`.
        fn stereo_index(&self) -> StereoIndex {
            unsafe_h3d! {
                mem::transmute(
                    ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::OutBufIndexI as i32)
                )
            }
        }

        /// Sets the index of the output buffer for stereoscopy rendering.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DCamera::OutBufIndexI`.
        fn set_stereo_index(&mut self, index: StereoIndex) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::OutBufIndexI as i32, index as i32)
            }
        }

        /// Gets the viewport rectangle.
        ///
        /// Returns the `(lower_left_x, lower_left_y, width, height)`.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DCamera::ViewportXI`, `H3DCamera::ViewportYI`,
        /// `H3DCamera::ViewportWidthI` and `H3DCamera::ViewportHeightI`.
        fn viewport(&self) -> (u32, u32, u32, u32) {
            unsafe_h3d! {(
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::ViewportXI as i32) as u32,
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::ViewportYI as i32) as u32,
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::ViewportWidthI as i32) as u32,
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::ViewportHeightI as i32) as u32,
            )}
        }

        /// Sets the viewport rectangle.
        ///
        /// `x` and `y` specifies the lower left corner of the viewport rectangle
        /// and `width` and `height` the dimensions of the rectangle.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DCamera::ViewportXI`, `H3DCamera::ViewportYI`,
        /// `H3DCamera::ViewportWidthI` and `H3DCamera::ViewportHeightI`.
        fn set_viewport(&mut self, x: u32, y: u32, width: u32, height: u32) {
            unsafe_h3d!({
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::ViewportXI as i32, x as i32);
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::ViewportYI as i32, y as i32);
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::ViewportWidthI as i32, width as i32);
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::ViewportHeightI as i32, height as i32);
            });
        }

        /// Gets the camera frustum.
        ///
        /// Returns the `(left, right, bottom, top, near_clip, far_clip)`.
        /// Default is (-0.055228457, 0.055228457, -0.041421354, 0.041421354, 0.1, 1000.0).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DCamera::LeftPlaneF`, `H3DCamera::RightPlaneF`,
        /// `H3DCamera::BottomPlaneF`, `H3DCamera::TopPlaneF`, `H3DCamera::NearPlaneF` and `H3DCamera::FarPlaneF`.
        fn frustum(&self) -> (f32, f32, f32, f32, f32, f32) {
            unsafe_h3d! {(
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DCamera::LeftPlaneF as i32, 0),
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DCamera::RightPlaneF as i32, 0),
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DCamera::BottomPlaneF as i32, 0),
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DCamera::TopPlaneF as i32, 0),
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DCamera::NearPlaneF as i32, 0),
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DCamera::FarPlaneF as i32, 0),
            )}
        }

        /// Sets the camera frustum.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DCamera::LeftPlaneF`, `H3DCamera::RightPlaneF`,
        /// `H3DCamera::BottomPlaneF`, `H3DCamera::TopPlaneF`, `H3DCamera::NearPlaneF` and `H3DCamera::FarPlaneF`.
        fn set_frustum(&mut self, left: f32, right: f32, bottom: f32, top: f32, near: f32, far: f32) {
            unsafe_h3d!({
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DCamera::LeftPlaneF as i32, 0, left);
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DCamera::RightPlaneF as i32, 0, right);
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DCamera::BottomPlaneF as i32, 0, bottom);
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DCamera::TopPlaneF as i32, 0, top);
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DCamera::NearPlaneF as i32, 0, near);
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DCamera::FarPlaneF as i32, 0, far);
            });
        }

        /// Sets the planes of a camera viewing frustum.
        ///
        /// This function calculates the view frustum planes of the specified camera node using the specified view parameters.
        /// This is essentially an alternative to `set_frustum`.
        ///
        /// Wrapper for `h3dSetupCameraView`.
        fn setup_camera_view(&mut self, fov: f32, aspect: f32, near: f32, far: f32) {
            unsafe_h3d! {
                ffi::h3dSetupCameraView(self.as_mut_node().raw(), fov, aspect, near, far)
            };
        }

        /// Gets the camera projection matrix.
        ///
        /// This function gets the camera projection matrix used for bringing the geometry to screen space
        ///
        /// Wrapper for `h3dGetCameraProjMat`.
        fn projection_matrix(&self) -> [f32; 16] {
            unsafe_h3d!({
                let mut matrix: [f32; 16] = { mem::uninitialized() };
                ffi::h3dGetCameraProjMat(self.as_node().raw(), &mut matrix[0]);
                matrix
            })
        }

        /// Gets whether an orthographic frustum instead of a perspective one is being used.
        ///
        /// Default is `false`.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DCamera::OrthoI`.
        fn is_ortho(&self) -> bool {
            unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::OrthoI as i32) != 0
            }
        }

        /// Sets whether an orthographic frustum instead of a perspective one is being used.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DCamera::OrthoI`.
        fn set_ortho(&mut self, is_ortho: bool) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::OrthoI as i32, is_ortho as i32)
            };
        }

        /// Gets whether occlusion culling is enabled.
        ///
        /// Default is `false`.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DCamera::OccCullingI`.
        fn is_occl(&self) -> bool {
            unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DCamera::OccCullingI as i32) != 0
            }
        }

        /// Sets whether occlusion culling is enabled.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DCamera::OccCullingI`.
        fn set_occl(&mut self, is_occl: bool) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DCamera::OccCullingI as i32, is_occl as i32)
            };
        }

        /// Renders the camera point of view into the output buffer.
        ///
        /// This is the main function of the engine. It executes all the rendering, animation and other tasks.
        /// The function can be called several times per frame, for example in order to write to
        /// different output buffers.
        ///
        /// Wrapper for `h3dRender`.
        fn render(&mut self) {
            unsafe_h3d! { ffi::h3dRender(self.as_mut_node().raw()) };
        }
    }
);


/////////////////////////////
// Emitter-specific data
//

impl_node!(EmitterNodeData, EmitterNodeDataMut, EmitterNode, EmitterNodeMut, EmitterNodeFns,

    pub trait EmitterNodeFns : NodeWrapper {

        /// Gets the material resource used for rendering the particles.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DEmitter::MatResI`.
        fn material(&self) -> Res {
            let handle = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DEmitter::MatResI as i32)
            };
            Res::from_raw(handle)
        }

        /// Sets the material resource used for rendering the particles.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DEmitter::MatResI`.
        fn set_material(&mut self, material: &MatRes) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DEmitter::MatResI as i32, material.as_res().raw())
            };
        }

        /// Gets the particle effect resource which configures particle properties.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DEmitter::PartEffResI`.
        fn particle_effect(&self) -> Res {
            let handle = unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DEmitter::PartEffResI as i32)
            };
            Res::from_raw(handle)
        }

        /// Sets the particle effect resource which configures particle properties.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DEmitter::PartEffResI`.
        fn set_particle_effect(&mut self, particle_fx: &PartEffRes) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DEmitter::PartEffResI as i32, particle_fx.as_res().raw())
            };
        }

        /// Gets the maximum number of particles living at the same time.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DEmitter::MaxCountI`.
        fn max_particles(&self) -> u32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DEmitter::MaxCountI as i32) as u32
            }
        }

        /// Sets the maximum number of particles living at the same time.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DEmitter::MaxCountI`.
        fn set_max_particles(&mut self, value: u32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DEmitter::MaxCountI as i32, value as i32)
            }
        }

        /// Gets the number of times a single particle is recreated after dying or `None` for infinite.
        ///
        /// Wrapper for `h3dGetNodeParamI` with `H3DEmitter::RespawnCountI`.
        fn respawn_count(&self) -> Option<i32> {
            unsafe_h3d! {
                match ffi::h3dGetNodeParamI(self.as_node().raw(), ffi::H3DEmitter::RespawnCountI as i32) {
                    -1 => None,
                    x => Some(x),
                }
            }
        }

        /// Sets the number of times a single particle is recreated after dying or `None` for infinite.
        ///
        /// Wrapper for `h3dSetNodeParamI` with `H3DEmitter::RespawnCountI`.
        fn set_respawn_count(&mut self, value: Option<i32>) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamI(self.as_mut_node().raw(), ffi::H3DEmitter::RespawnCountI as i32, value.unwrap_or(-1))
            }
        }

        /// Gets the time in seconds before emitter begins creating particles (default is 0.0).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DEmitter::DelayF`.
        fn delay(&self) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DEmitter::DelayF as i32, 0)
            }
        }

        /// Sets the time in seconds before emitter begins creating particles.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DEmitter::DelayF`.
        fn set_delay(&mut self, value: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DEmitter::DelayF as i32, 0, value)
            }
        }

        /// Gets the maximal number of particles to be created per second (default is 0.0).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DEmitter::EmissionRateF`.
        fn emission_rate(&self) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DEmitter::EmissionRateF as i32, 0)
            }
        }

        /// Sets the maximal number of particles to be created per second.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DEmitter::EmissionRateF`.
        fn set_emission_rate(&mut self, value: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DEmitter::EmissionRateF as i32, 0, value)
            }
        }

        /// Gets the angle of cone for random emission direction (default is 0.0).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DEmitter::SpreadAngleF`.
        fn spread_angle(&self) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetNodeParamF(self.as_node().raw(), ffi::H3DEmitter::SpreadAngleF as i32, 0)
            }
        }

        /// Sets the angle of cone for random emission direction.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DEmitter::SpreadAngleF`.
        fn set_spread_angle(&mut self, value: f32) {
            unsafe_h3d! {
                ffi::h3dSetNodeParamF(self.as_mut_node().raw(), ffi::H3DEmitter::SpreadAngleF as i32, 0, value)
            }
        }

        /// Gets the force vector XYZ applied to particles (default is [0.0, 0.0, 0.0]).
        ///
        /// Wrapper for `h3dGetNodeParamF` with `H3DEmitter::ForceF3`.
        fn force(&self) -> [f32; 3] {
            unsafe_h3d!({
                let mut xyz: [f32; 3] = { mem::uninitialized() };
                h3dGetNodeParamFN(self.as_node().raw(), ffi::H3DEmitter::ForceF3 as i32, 3, &mut xyz);
                xyz
            })
        }

        /// Sets the force vector XYZ applied to particles.
        ///
        /// Wrapper for `h3dSetNodeParamF` with `H3DEmitter::ForceF3`.
        fn set_force(&mut self, xyz: &[f32; 3]) {
            unsafe_h3d!({
                h3dSetNodeParamFN(self.as_mut_node().raw(), ffi::H3DEmitter::ForceF3 as i32, 3, xyz);
            })
        }

        /// Advances the time value of an emitter node.
        ///
        /// This function advances the simulation time of a particle system and continues the
        /// particle simulation with `time_delta` being the time elapsed since the last call of
        /// this function.
        ///
        /// Wrapper for `h3dUpdateEmitter` (known previously as `h3dAdvanceEmitterTime`).
        fn update(&mut self, time_delta: f32) {
            unsafe_h3d! {
                ffi::h3dUpdateEmitter(self.as_mut_node().raw(), time_delta)
            };
        }

        /// Checks if an emitter node is still alive.
        ///
        /// This function checks if a particle system is still active and has living particles or
        /// will spawn new particles.
        ///
        /// The function can be used to check when a not infinitely running emitter for an effect
        /// like an explosion can be removed from the scene.
        ///
        /// Wrapper for `h3dHasEmitterFinished`.
        fn has_finished(&self) -> bool {
            unsafe_h3d! {
                ffi::h3dHasEmitterFinished(self.as_node().raw()) != 0
            }
        }
    }
);



/*
TODO maybe move it back to Node

    /// Gets the relative translation of a node.
    /// See `transform` for details.
    ///
    /// Wrapper for `h3dGetNodeTransform` taking only the translation.
    pub fn translation(&self) -> [f32; 3] {
        unsafe_h3d!({
            let mut t: [f32; 3] = { mem::uninitialized() };
            ffi::h3dGetNodeTransform(self.raw(),
                                     &mut t[0], &mut t[1], &mut t[2],
                                     ptr::null_mut(), ptr::null_mut(), ptr::null_mut(),
                                     ptr::null_mut(), ptr::null_mut(), ptr::null_mut());
            t
        })
    }

    /// Gets the relative rotation of a node.
    /// See `transform` for details.
    ///
    /// Wrapper for `h3dGetNodeTransform` taking only the rotation.
    pub fn rotation(&self) -> [f32; 3] {
        unsafe_h3d!({
            let mut r: [f32; 3] = { mem::uninitialized() };
            ffi::h3dGetNodeTransform(self.raw(),
                                     ptr::null_mut(), ptr::null_mut(), ptr::null_mut(),
                                     &mut r[0], &mut r[1], &mut r[2],
                                     ptr::null_mut(), ptr::null_mut(), ptr::null_mut());
            r
        })
    }

    /// Gets the relative scale of a node.
    /// See `transform` for details.
    ///
    /// Wrapper for `h3dGetNodeTransform` taking only the scale.
    pub fn scale(&self) -> [f32; 3] {
        unsafe_h3d!({
            let mut s: [f32; 3] = { mem::uninitialized() };
            ffi::h3dGetNodeTransform(self.raw(),
                                     ptr::null_mut(), ptr::null_mut(), ptr::null_mut(),
                                     ptr::null_mut(), ptr::null_mut(), ptr::null_mut(),
                                     &mut s[0], &mut s[1], &mut s[2]);
            s
        })
    }
*/
