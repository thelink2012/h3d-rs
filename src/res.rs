use std::{mem, ptr, slice};
use std::ffi::{CStr};
use std::cell::{RefCell, Cell};
use std::marker::PhantomData;
use ffi;
use util::{get_error_string, get_error_flag, with_c_str, string_from_c_str};
use Result;

// TODO h3dGetNextResource
// TODO h3dQueryUnloadedResource

#[repr(i32)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
pub enum ResType {
    Undefined = 0,      // XXX https://github.com/rust-lang/rust/issues/24469
    SceneGraph,
    Geometry,
    Animation,
    Material,
    Code,
    Shader,
    Texture,
    ParticleEffect,
    Pipeline,
}

bitflags! {
    flags ResFlags: i32 {
        const NO_QUERY  = ffi::H3DResFlags::NoQuery as i32,
    }
}

bitflags! {
    flags TexResFlags: i32 {
        const NO_TEX_COMPRESSION = ffi::H3DResFlags::NoTexCompression as i32,
        const NO_TEX_MIPMAPS     = ffi::H3DResFlags::NoTexMipmaps as i32,
        const TEX_CUBEMAP        = ffi::H3DResFlags::TexCubemap as i32,
        const TEX_DYNAMIC        = ffi::H3DResFlags::TexDynamic as i32,
        const TEX_RENDERABLE     = ffi::H3DResFlags::TexRenderable as i32,
        const TEX_SRGB           = ffi::H3DResFlags::TexSRGB as i32,
    }
}

/// Represents an engine resource.
///
/// Wrapper for `H3DRes` type.
#[derive(Debug)]
pub struct Res(ffi::H3DRes);

impl Res {

    /// Creates a new resource.
    ///
    /// This function tries to add a resource of a specified `kind` and `name` to the resource manager.
    /// If a resource of the same type and name is already existing, returns `Err`.
    ///
    /// The `raw_flags` parameter is a representation of the possible merge of `ResFlags` and `TexResFlags` bits.
    ///
    /// It's strongly recommended to use `new_scene_graph`, `new_geometry`, `new_animation`, `new_material`,
    /// `new_code`, `new_shader`, `new_texture`, `new_particle_effect` and `new_pipeline` instead of this.
    ///
    /// **Note:** Unlike Horde3D C API, the Rust binding does not return successfully when a resource
    /// named `name` already exists.
    ///
    /// Wrapper for `h3dAddResource`.
    pub fn new(kind: ResType, name: &str, raw_flags: i32) -> Result<Res> {
        // XXX is the change to not allow previosly named resources okay?
        // Note how the C API uses this to increase the ref count though our Rust version of find
        // also increments the reference counter, so we should recommend using find before calling this?
        with_c_str(name, |cname| {
            unsafe_h3d!({
                let handle = ffi::h3dFindResource(kind as i32, cname);
                if handle != 0 {
                    Err("Failed to create resource, name already exists".to_string())
                } else {
                    let handle = ffi::h3dAddResource(kind as i32, cname, raw_flags);
                    if handle == 0 {
                        Err(get_error_string())
                    } else {
                        Ok(Res::from_raw_noref(handle))
                    }
                }
            })
        })
    }

    /// Creates a new scene graph resource.
    /// See `Res::new` for details.
    pub fn new_scene_graph(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::SceneGraph, name, flags.bits())
    }

    /// Creates a new geometry resource.
    /// See `Res::new` for details.
    pub fn new_geometry(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::Geometry, name, flags.bits())
    }

    /// Creates a new animation resource.
    /// See `Res::new` for details.
    pub fn new_animation(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::Animation, name, flags.bits())
    }

    /// Creates a new material resource.
    /// See `Res::new` for details.
    pub fn new_material(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::Material, name, flags.bits())
    }

    /// Creates a new code resource.
    /// See `Res::new` for details.
    pub fn new_code(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::Code, name, flags.bits())
    }

    /// Creates a new shader resource.
    /// See `Res::new` for details.
    pub fn new_shader(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::Shader, name, flags.bits())
    }

    /// Creates a new texture resource.
    /// See `Res::new` for details.
    pub fn new_texture(name: &str, flags: ResFlags, texflags: TexResFlags) -> Result<Res> {
        Res::new(ResType::Texture, name, flags.bits() | texflags.bits())
    }

    /// Creates a new particle effect resource.
    /// See `Res::new` for details.
    pub fn new_particle_effect(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::ParticleEffect, name, flags.bits())
    }

    /// Creates a new pipeline resource.
    /// See `Res::new` for details.
    pub fn new_pipeline(name: &str, flags: ResFlags) -> Result<Res> {
        Res::new(ResType::Pipeline, name, flags.bits())
    }

    /// Creates a new texture resource.
    ///
    /// This function tries to create and add a texture resource with the specified name to the resource manager.
    /// If a texture resource with the same `name` is already existing, the function fails.
    /// The texture is initialized with the specified dimensions (`width` by `height`)
    /// and the resource is declared as loaded.
    ///
    /// This function is especially useful to create dynamic textures (e.g. for displaying videos)
    /// or output buffers for render-to-texture.
    ///
    /// Wrapper for `h3dCreateTexture`.
    pub fn new_texture_raw(name: &str, width: usize, height: usize, fmt: TexFormat, // TODO better function name?
                           flags: ResFlags, texflags: TexResFlags) -> Result<Res> {
        with_c_str(name, |cname| {
            let handle = unsafe_h3d! {
                ffi::h3dCreateTexture(cname, width as i32, height as i32,
                                      fmt as i32, flags.bits() | texflags.bits())
            };
            if handle == 0 { Err(get_error_string()) } else { Ok(Res::from_raw(handle)) }
        })
    }

    /// Adds to the reference counter of the specified resource and returns another container for it.
    pub fn from_res(res: &Res) -> Res {
        unsafe { Res::from_raw(res.raw()) }
    }

    /// Takes a owned `Res` from a non-owned `H3DRes`.
    ///
    /// Some Horde3D functions returns a H3DRes handle on which doesn't add to the Horde reference counter.
    /// Since we need to take ownership and make sure those resources aren't freed before `Res` we need to
    /// call h3dAddResource on the resource again so we increment the reference count for it.
    ///
    /// # Panics
    ///
    /// Panics if the `cres` handle is invalid.
    ///
    pub fn from_raw(cres: ffi::H3DRes) -> Res {
        unsafe_h3d!({
            assert!(cres != 0);
            let (ctype, cname) = (ffi::h3dGetResType(cres), ffi::h3dGetResName(cres));
            assert!(ctype != ffi::H3DResTypes::Undefined as i32);
            debug_assert!(CStr::from_ptr(cname).to_bytes().len() > 0);
            let cnewres = ffi::h3dAddResource(ctype, cname, 0);
            assert!(cnewres == cres);
            Res::from_raw_noref(cres)
        })
    }

    /// Takes the ownership of a owned `H3DRes` into a owned `Res`.
    ///
    /// See `Res::from_raw`.
    ///
    /// # Safety
    ///
    /// The user must make sure the passed resource handle has a reference counter specifically for it.
    /// The handle is going to be then owned by the returned `Res` meaning it will decrementing
    /// the reference counter on drop.
    ///
    #[inline]
    pub unsafe fn from_raw_noref(cres: ffi::H3DRes) -> Res {
        assert!(cres != 0);
        Res(cres)
    }

    /// Gets the contained `H3DRes` handle.
    ///
    /// # Safety
    ///
    /// What do you want with this handle?
    /// In any case, be careful on what you do with the returned handle!
    /// Specifically **DO NOT** reduce it's reference counter.
    ///
    #[inline]
    pub unsafe fn raw(&self) -> ffi::H3DRes {
        self.0
    }



    /// Returns the type of a resource.
    ///
    /// Wrapper for `h3dGetResType`.
    pub fn kind(&self) -> ResType {
        let result: ResType = unsafe_h3d! { mem::transmute(ffi::h3dGetResType(self.raw())) };
        assert!(result != ResType::Undefined);
        result
    }

    /// Returns the name of a resource.
    ///
    /// Wrapper for `h3dGetResName`.
    pub fn name(&self) -> String {
        let result: String = unsafe_h3d! { string_from_c_str(ffi::h3dGetResName(self.raw())) };
        assert!(!result.is_empty());
        result
    }

    /// Checks if a resource is loaded.
    ///
    /// This function checks if the specified resource has been successfully loaded.
    ///
    /// Wrapper for `h3dIsResLoaded`.
    pub fn is_loaded(&self) -> bool {
        unsafe_h3d! { ffi::h3dIsResLoaded(self.raw()) != 0 }
    }

    /// Loads a resource.
    ///
    /// This function loads `data` into this `Res` object.
    /// If `data` is `None` the resource manager is told that the resource doesn’t have any data
    /// (e.g. the corresponding file was not found). In this case, the resource remains in the unloaded
    /// state but is no more returned when querying unloaded resources with `TODO`.
    ///
    /// When the specified resource is already loaded, the function returns `Err`.
    /// The function always succeeds when `data` is `None`.
    ///
    /// Wrapper for `h3dLoadResource`.
    pub fn load(&mut self, data: Option<&[u8]>) -> Result<()> {
        unsafe_h3d!({
            use libc::{c_int, c_char};
            let is_okay = match data {
                Some(a) => ffi::h3dLoadResource(self.raw(), a.as_ptr() as *const c_char, a.len() as c_int) != 0,
                None => ffi::h3dLoadResource(self.raw(), ptr::null(), 0) == 0, // that's correct
            };
            if !is_okay { Err(get_error_string()) } else { Ok(()) }
        })
    }

    /// Unloads a resource.
    ///
    /// This function unloads a previously loaded resource and restores the default values it had before loading.
    /// The state is set back to unloaded which makes it possible to load the resource again.
    ///
    /// Wrapper for `h3dUnloadResource`.
    pub fn unload(&mut self) {
        unsafe_h3d! { ffi::h3dUnloadResource(self.raw()) };
    }

    /// Clones the resource naming the new resource with the specified `name`.
    ///
    /// In the cloning process a new resource with the specified name is added to the resource manager
    /// and filled with the data of the specified source resource.
    ///
    /// If the `name` string is empty, a unique name for the resource is generated automatically.
    ///
    /// # Failures
    /// If the specified name for the new resource is already in use, the function fails.
    ///
    /// Wrapper for `h3dCloneResource`.
    pub fn clone_named(&self, name: &str) -> Result<Res> {
        with_c_str(name, |cname| {
            unsafe_h3d!({
                let handle = ffi::h3dCloneResource(self.raw(), cname);
                if handle == 0 {
                    Err(get_error_string())
                } else {
                    Ok(Res::from_raw_noref(handle))
                }
            })
        })
    }

    /// Finds a resource named `name`.
    ///
    /// This function searches for the resource of the specified type and name and returns it.
    ///
    /// Wrapper for `h3dFindResource`.
    pub fn find(kind: ResType, name: &str) -> Option<Res> {
        with_c_str(name, |cname| {
            unsafe_h3d!({
                let handle = ffi::h3dFindResource(kind as i32, cname);
                if handle == 0 {
                    None
                } else {
                    Some(Res::from_raw(handle))
                }
            })
        })
    }


    /// Gets a reference to the scene graph specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a scene graph resource.
    ///
    pub fn as_scene_graph(&self) -> SceneGraphResData {
        assert_eq!(self.kind(), ResType::SceneGraph);
        SceneGraphResData { res: self }
    }

    /// Gets a reference to the mutable scene graph specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a scene graph resource.
    ///
    pub fn as_scene_graph_mut(&mut self) -> SceneGraphResDataMut {
        assert_eq!(self.kind(), ResType::SceneGraph);
        SceneGraphResDataMut { res: self }
    }

    /// Gets a reference to the geometry specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a geometry resource.
    ///
    pub fn as_geometry(&self) -> GeoResData {
        assert_eq!(self.kind(), ResType::Geometry);
        GeoResData { res: self }
    }

    /// Gets a reference to the mutable geometry specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a geometry resource.
    ///
    pub fn as_geometry_mut(&mut self) -> GeoResDataMut {
        assert_eq!(self.kind(), ResType::Geometry);
        GeoResDataMut { res: self }
    }

    /// Gets a reference to the animation specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a animation resource.
    ///
    pub fn as_animation(&self) -> AnimResData {
        assert_eq!(self.kind(), ResType::Animation);
        AnimResData { res: self }
    }

    /// Gets a reference to the mutable animation specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a animation resource.
    ///
    pub fn as_animation_mut(&mut self) -> AnimResDataMut {
        assert_eq!(self.kind(), ResType::Animation);
        AnimResDataMut { res: self }
    }

    /// Gets a reference to the material specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a material resource.
    ///
    pub fn as_material(&self) -> MatResData {
        assert_eq!(self.kind(), ResType::Material);
        MatResData { res: self }
    }

    /// Gets a reference to the mutable material specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a material resource.
    ///
    pub fn as_material_mut(&mut self) -> MatResDataMut {
        assert_eq!(self.kind(), ResType::Material);
        MatResDataMut { res: self }
    }

    /// Gets a reference to the code specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a code resource.
    ///
    pub fn as_code(&self) -> CodeResData {
        assert_eq!(self.kind(), ResType::Code);
        CodeResData { res: self }
    }

    /// Gets a reference to the mutable code specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a code resource.
    ///
    pub fn as_code_mut(&mut self) -> CodeResDataMut {
        assert_eq!(self.kind(), ResType::Code);
        CodeResDataMut { res: self }
    }

    /// Gets a reference to the shader specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a shader resource.
    ///
    pub fn as_shader(&self) -> ShaderResData {
        assert_eq!(self.kind(), ResType::Shader);
        ShaderResData { res: self }
    }

    /// Gets a reference to the mutable shader specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a shader resource.
    ///
    pub fn as_shader_mut(&mut self) -> ShaderResDataMut {
        assert_eq!(self.kind(), ResType::Shader);
        ShaderResDataMut { res: self }
    }

    /// Gets a reference to the texture specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a texture resource.
    ///
    pub fn as_texture(&self) -> TexResData {
        assert_eq!(self.kind(), ResType::Texture);
        TexResData { res: self }
    }

    /// Gets a reference to the mutable texture specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a texture resource.
    ///
    pub fn as_texture_mut(&mut self) -> TexResDataMut {
        assert_eq!(self.kind(), ResType::Texture);
        TexResDataMut { res: self }
    }

    /// Gets a reference to the particle_effect specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a particle_effect resource.
    ///
    pub fn as_particle_effect(&self) -> PartEffResData {
        assert_eq!(self.kind(), ResType::ParticleEffect);
        PartEffResData { res: self }
    }

    /// Gets a reference to the mutable particle_effect specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a particle_effect resource.
    ///
    pub fn as_particle_effect_mut(&mut self) -> PartEffResDataMut {
        assert_eq!(self.kind(), ResType::ParticleEffect);
        PartEffResDataMut { res: self }
    }

    /// Gets a reference to the pipeline specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a pipeline resource.
    ///
    pub fn as_pipeline(&self) -> PipeResData {
        assert_eq!(self.kind(), ResType::Pipeline);
        PipeResData { res: self }
    }

    /// Gets a reference to the mutable pipeline specific data of this resource.
    ///
    /// # Panics
    /// Panics if the resource is not a pipeline resource.
    ///
    pub fn as_pipeline_mut(&mut self) -> PipeResDataMut {
        assert_eq!(self.kind(), ResType::Pipeline);
        PipeResDataMut { res: self }
    }
}

impl Clone for Res {
    /// Duplicates a resource.
    ///
    /// The `Res::name()` of the duplicated resource is unespecified, use `Res::clone_named` if you
    /// wish to specify a name to it.
    ///
    /// Wrapper for `h3dCloneResource`.
    fn clone(&self) -> Res {
        self.clone_named("").unwrap()
    }
}

impl Drop for Res {
    /// Decreases the user reference count of a specified resource.
    ///
    /// Wrapper for `h3dRemoveResource`
    fn drop(&mut self) {
        assert!(unsafe_h3d! { ffi::h3dRemoveResource(self.raw()) != -1 });
    }
}


/////////////////////////////////////////////////////////////////////
// As follow are resource specific data and streams.
//

/////////////////////
// TODO explain somewhere about the information contained in this:
//
// This function maps the specified stream of a specified resource element and
// returns a pointer to the stream data.  The required access to the data can be specified
// with the read write parameters.  If read is false, the pointer will
// usually not contain meaningful data.  Not all resource streams can be
// mapped with both read and write access.  If it is not possible to map the stream,
// the function will return a NULL pointer.
//
// A mapped stream should be unmapped again as soon as possible but always before subsequent
// API calls are made.  It is only possible to map one stream per resource at a time.
//
/////////////////////

/// Implements a resource specific data accessor.
///
/// This macro is used to implement the immutable and mutable specific data accessors.
///
/// The `$WrapperName` and `$WrapperNameMut` are the name for the actual data structures to to wrap the Res.
/// The `$TraitName` and `$TraitNameMut` are traits implemented by those types so the user can enforce a resource
/// type in function arguments.
///
/// The `$StreamType` is a path to the resource mapper of this resource, without generic args since
/// it'll use the generic args from inside.
///
/// the `$ImplBlock` is a implementation of the trait named `$ImplName` which, this trait should
/// contain all the resource specific accessors and setters.
///
/// At the end of the day this macro will generate the following:
///
///   * `$ImplName` (`^ResFns`) trait, implemented from the `$ImplBlock` macro parameter.
///   * `$WrapperName` and `$WrapperNameMut` (`^ResData`) structs, which implements `^ResFns` and `ResWrapper` traits.
///     Those types are wrappers to a reference into the `Res` object that'll be accessed or modified.
///     The immutable version panics if `ResWrapper::as_mut_res` is called for it.
///   * The implementation of `^ResData` defines an `as_stream` method which returns a
///     `$StreamType<'a, ^ResData>` object with the ownership of the wrapper moved into it.
///
/// The generated types, `^ResData` should be used in the return type of
/// `Res::as_<restype>` and `Res::as_<restype>_mut`.
///
macro_rules! impl_res {
    ($WrapperName:ident, $WrapperNameMut:ident,
     $TraitName:ident, $TraitNameMut:ident,
     $StreamType:ident, $ImplName:ident, $ImplBlock:item) => {

        /// Implements acessors and setters for resource specific data.
        ///
        $ImplBlock

        /// A accessor to resource specific data.
        pub struct $WrapperName<'a> {
            pub res: &'a Res,
        }

        /// A accessor and setter to resource specific data.
        pub struct $WrapperNameMut<'a> {
            pub res: &'a mut Res,
        }


        /// Trait implemented by the immutable and the mutable resource accessors.
        pub trait $TraitName where Self: ResWrapper, Self: $ImplName {
            // ...
        }

        /// Trait implemented by the mutable resource accessor.
        pub trait $TraitNameMut where Self: $TraitName {
            // ...
        }

        // Implements `$TraitName` and `$TraitNameMut`.
        impl<'a> $TraitName for $WrapperName<'a> { }
        impl<'a> $TraitName for $WrapperNameMut<'a> { }
        impl<'a> $TraitNameMut for $WrapperNameMut<'a> { }

        // Implements `$ImplBlock`.
        impl<'a> $ImplName for $WrapperName<'a> { }
        impl<'a> $ImplName for $WrapperNameMut<'a> { }

        impl<'a> ResWrapper for $WrapperName<'a> {
            /// Gets a reference to the wrapped resource.
            fn as_res(&self) -> &Res {
                self.res
            }

            /// **Panics** since as this is a immutable wrapper.
            fn as_mut_res(&mut self) -> &mut Res {
                panic!("Immutable value trying to be mutable");
            }
        }

        impl<'a> ResWrapper for $WrapperNameMut<'a> {
            /// Gets a reference to the wrapped resource.
            fn as_res(&self) -> &Res {
                self.res
            }

            /// Gets a mutable reference to the wrapped resource.
            fn as_mut_res(&mut self) -> &mut Res {
                self.res
            }
        }

        #[allow(dead_code)] // TODO remove attrib?
        impl<'a> $WrapperName<'a> {
            /// Turns into a mapper of the resource data into the memory.
            pub fn as_stream(self) -> $StreamType<'a, $WrapperName<'a>> {
                $StreamType::new(self)
            }
        }

        #[allow(dead_code)] // TODO remove attrib?
        impl<'a> $WrapperNameMut<'a> {
            /// Turns into a mapper of the resource data into the memory.
            pub fn as_stream(self) -> $StreamType<'a, $WrapperNameMut<'a>> {
                $StreamType::new(self)
            }
        }
    }
}

/// Implements a resource mapper that does nothing.
///
/// Used for material streams, pipeline streams and other resources that do not actually have a stream.
macro_rules! impl_dummy_res_stream {
    ($ResStreamName:ident) => {
        /// Dummy mapper, has no use.
        pub struct $ResStreamName<'a, TRes: ResWrapper> {
            stream: UnsafeResStream<TRes>,
            phantom: PhantomData<&'a i32>,
        }

        /// Has no use.
        impl<'a, TRes: ResWrapper> $ResStreamName<'a, TRes> {
            pub fn new(res: TRes) -> $ResStreamName<'a, TRes> {
                $ResStreamName {
                    stream: UnsafeResStream::new(res),
                    phantom: PhantomData,
                }
            }
        }
    }
}

/// Wraps a resource.
///
/// This is implemented by resource specific accessors.
pub trait ResWrapper {
    /// Gets a reference to the wrapped resource.
    fn as_res(&self) -> &Res;
    /// Gets a mutable reference to the wrapped resource or **panics** if the wrapper is not mutable.
    fn as_mut_res(&mut self) -> &mut Res;
}

// The core primitive for resource mapping.
//
// This type is a RAII wrapper around a resource mapping.
// When this gets dropped it unmaps the resource if it was mapped before.
//
pub struct UnsafeResStream<TRes: ResWrapper> {

    // To allow mutation in a immutable state we are using `Cell` and `RefCell` on here.

    /// The mapped resource.
    ///
    /// This field should not be accessed directly, it is made public for the safe mappers.
    pub res: RefCell<TRes>,
    is_mapped: Cell<bool>,
}

impl<TRes: ResWrapper> UnsafeResStream<TRes> {

    /// Constructs a new instance of `UnsafeResStream` which wraps mapping for a resource.
    ///
    /// All access to this resource is unsafe, please use the safe wrappers to this object.
    pub fn new(res: TRes) -> UnsafeResStream<TRes> {
        UnsafeResStream {
            res: RefCell::new(res),
            is_mapped: Cell::new(false),
        }
    }

    /// Wrapper for `h3dMapResStream`.
    ///
    /// This never returns a null pointer.
    ///
    /// # Safety
    ///
    /// This method returns a mutable pointer even though it may not be mutated depending on the `write` parameter.
    /// It also mutates internal data even though the immutable `&self` specifier says otherwise.
    ///
    /// # Panics
    ///
    /// Panics if the `write` access has been asked but the wrapped resource is immutable.
    /// Also panics in case this mapper is already mapped.
    ///
    pub unsafe fn raw_map(&self, elem: i32, elem_idx: i32, stream: i32, read: bool, write: bool) -> Result<*mut u8> {
        assert_eq!(self.is_mapped.get(), false);

        let handle = if write { self.res.borrow_mut().as_mut_res().raw() } else { self.res.borrow().as_res().raw() };
        let pointer: *mut u8 = unsafe_h3d! { ffi::h3dMapResStream(handle, elem, elem_idx, stream, read as i8, write as i8) as *mut u8 };

        if pointer.is_null() {

            if !get_error_flag() {
                // Not a error, just the pointer is not available neither internally.
                // Let's just unmap again (necessary).
                self.is_mapped.set(true);
                self.raw_unmap();
                Err("Request data to be mapped not available".to_string())
            } else {
                Err(get_error_string())
            }

        } else {
            self.is_mapped.set(true);
            Ok(pointer)
        }
    }

    /// Wrapper for `h3dUnmapResStream`.
    ///
    /// # Safety
    ///
    /// This method may mutate the state of the wrapped resource even though the immutable `&self`
    /// specifier says otherwise.
    ///
    /// # Panics
    ///
    /// Panics in case the mapper is not mapped.
    ///
    pub unsafe fn raw_unmap(&self) {
        assert_eq!(self.is_mapped.get(), true);
        unsafe_h3d! { ffi::h3dUnmapResStream(self.res.borrow().as_res().raw()) };
        self.is_mapped.set(false);
    }
}

impl<TRes: ResWrapper> Drop for UnsafeResStream<TRes> {
    /// Unmaps the stream if mapped.
    fn drop(&mut self) {
        if self.is_mapped.get() {
            unsafe { self.raw_unmap(); }
        }
    }
}

#[allow(non_snake_case)]
unsafe fn h3dGetResParamFN(res: i32, elem: i32, elem_idx: i32, param: i32, count: usize, arr: &mut [f32]) {
    debug_assert!(arr.len() == count);
    for x in 0..count {
        arr[x] = ffi::h3dGetResParamF(res, elem, elem_idx, param, x as i32);
    }
}

#[allow(non_snake_case)]
unsafe fn h3dSetResParamFN(res: i32, elem: i32, elem_idx: i32, param: i32, count: usize, arr: &[f32]) {
    debug_assert!(arr.len() == count);
    for x in 0..count {
        ffi::h3dSetResParamF(res, elem, elem_idx, param, x as i32, arr[x]);
    }
}

#[allow(non_snake_case)]
unsafe fn h3dGetResParamStr(res: i32, elem: i32, elem_idx: i32, param: i32) -> String {
    string_from_c_str(ffi::h3dGetResParamStr(res, elem, elem_idx, param))
}

#[allow(non_snake_case)]
unsafe fn h3dSetResParamStr(res: i32, elem: i32, elem_idx: i32, param: i32, value: &str) {
    with_c_str(value, |cvalue| ffi::h3dSetResParamStr(res, elem, elem_idx, param, cvalue))
}



/////////////////////////////
// Geometry-specific data
//

/// Geometry indices format.
#[repr(i32)]
pub enum GeoIndexFormat {
    /// 16 bits indices.
    Fmt16,
    /// 32 bits indices.
    Fmt32,
}

/// Immutably mapped geometry.
pub enum GeoIndexMapped<'a> {
    /// 16 bits indices.
    Fmt16(&'a [u16]),
    /// 32 bits indices.
    Fmt32(&'a [u32]),
}

/// Mutably mapped geometry.
pub enum GeoIndexMappedMut<'a> {
    /// 16 bits indices.
    Fmt16(&'a mut [u16]),
    /// 32 bits indices.
    Fmt32(&'a mut [u32]),
}

impl_res!(GeoResData, GeoResDataMut, GeoRes, GeoResMut, GeoResStream, GeoResFns,

    /// Geometry specific accessors.
    pub trait GeoResFns : ResWrapper {

        /// Number of vertices in the geometry.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DGeoRes::GeometryElem` with `H3DGeoRes::GeoVertexCountI`.
        fn num_verts(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DGeoRes::GeometryElem as i32,
                                     0, ffi::H3DGeoRes::GeoVertexCountI as i32) as usize
            }
        }

        /// Number of indices in the geometry.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DGeoRes::GeometryElem` with `H3DGeoRes::GeoIndexCountI`.
        fn num_indices(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DGeoRes::GeometryElem as i32,
                                     0, ffi::H3DGeoRes::GeoIndexCountI as i32) as usize
            }
        }

        /// Whether geometry index data is 16 or 32 bits.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DGeoRes::GeometryElem` with `H3DGeoRes::GeoIndices16I`.
        fn indices_format(&self) -> GeoIndexFormat {
            unsafe_h3d!({
                if ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DGeoRes::GeometryElem as i32,
                                        0, ffi::H3DGeoRes::GeoIndices16I as i32) != 0 {
                    GeoIndexFormat::Fmt16
                } else {
                    GeoIndexFormat::Fmt32
                }
            })
        }

        // `as_stream` implemented in `TRes` and `TResMut` itself because of lifetimes.
    }
);

/// Geometry data mapper.
pub struct GeoResStream<'a, TRes: ResWrapper> {
    stream: UnsafeResStream<TRes>,
    phantom: PhantomData<&'a i32>,
}

impl<'a, TRes: GeoRes> GeoResStream<'a, TRes> {

    /// Constructs a geometry mapper.
    ///
    /// Don't use this directly, use the `.as_stream()` method of geometry accessors.
    ///
    pub fn new(res: TRes) -> GeoResStream<'a, TRes> {
        GeoResStream {
            stream: UnsafeResStream::new(res),
            phantom: PhantomData,
        }
    }

    /// Maps triangle index data into a memory location for reading.
    ///
    /// # Panics
    ///
    /// Panics in case this mapper is already mapped.
    ///
    pub fn map_indices(&self) -> Result<GeoIndexMapped> {
        let mapped = try!(unsafe { self.raw_map_indices(true, false) });
        match mapped {
            GeoIndexMappedMut::Fmt16(a) => Ok(GeoIndexMapped::Fmt16(a)),
            GeoIndexMappedMut::Fmt32(a) => Ok(GeoIndexMapped::Fmt32(a)),
        }
    }

    /// Maps triangle index data into a memory location for writing.
    ///
    /// In case of the need to read the mapped data, `allow_read` should be set to `true`
    /// otherwise the returned memory location may not contain meaningful data.
    ///
    /// # Panics
    ///
    /// Panics if the wrapped resource is immutable (`TRes` instead of `TResMut`).
    /// Also panics in case this mapper is already mapped.
    ///
    pub fn map_indices_mut(&mut self, allow_read: bool) -> Result<GeoIndexMappedMut> {
        unsafe { self.raw_map_indices(allow_read, true) }
    }

    /// Maps vertex position data into a memory location for reading.
    ///
    /// # Panics
    ///
    /// Panics in case this mapper is already mapped.
    ///
    pub fn map_verts_pos(&self) -> Result<&[f32]> { // TODO -> Vec3
        unsafe { self.raw_map_verts_pos(true, false) }
    }

    /// Maps vertex position data into a memory location for writing.
    ///
    /// In case of the need to read the mapped data, `allow_read` should be set to `true`
    /// otherwise the returned memory location may not contain meaningful data.
    ///
    /// # Panics
    ///
    /// Panics if the wrapped resource is immutable (`TRes` instead of `TResMut`).
    /// Also panics in case this mapper is already mapped.
    ///
    pub fn map_verts_pos_mut(&mut self, allow_read: bool) -> Result<&[f32]> { // TODO -> Vec3
        unsafe { self.raw_map_verts_pos(allow_read, true) }
    }

    // TODO doc
    pub fn map_verts_tan(&self) -> Result<&[f32]> { // TODO -> proper type
        unsafe { self.raw_map_verts_tan(true, false) }
    }
    pub fn map_verts_tan_mut(&self, allow_read: bool) -> Result<&[f32]> { // TODO -> proper type
        unsafe { self.raw_map_verts_tan(allow_read, true) }
    }

    // TODO doc
    pub fn map_verts_attrib(&self) -> Result<&[f32]> { // TODO -> proper type
        unsafe { self.raw_map_verts_attrib(true, false) }
    }
    pub fn map_verts_attrib_mut(&self, allow_read: bool) -> Result<&[f32]> { // TODO -> proper type
        unsafe { self.raw_map_verts_attrib(allow_read, true) }
    }

    /// Unmaps a previosly mapped data on this stream.
    ///
    /// Not necessary to call explicitly, this is called when the stream is dropped.
    ///
    /// # Panics
    ///
    /// Panics if the stream is not mapped to anything.
    ///
    pub fn unmap(&self) {
        // Panics if not mapped
        unsafe { self.stream.raw_unmap(); }
    }

    unsafe fn raw_map_indices(&self, read: bool, write: bool) -> Result<GeoIndexMappedMut> {
        let ptr = try!(self.stream.raw_map(ffi::H3DGeoRes::GeometryElem as i32, 0,
                                           ffi::H3DGeoRes::GeoIndexStream as i32, read, write));

        let res = self.stream.res.borrow();
        match res.indices_format() {
            GeoIndexFormat::Fmt16 => {
                Ok(GeoIndexMappedMut::Fmt16(slice::from_raw_parts_mut(ptr as *mut u16, res.num_indices())))
            },
            GeoIndexFormat::Fmt32 => {
                Ok(GeoIndexMappedMut::Fmt32(slice::from_raw_parts_mut(ptr as *mut u32, res.num_indices())))
            },
        }
    }

    unsafe fn raw_map_verts_pos(&self, read: bool, write: bool) -> Result<&[f32]> {
        let ptr = try!(self.stream.raw_map(ffi::H3DGeoRes::GeometryElem as i32, 0,
                                           ffi::H3DGeoRes::GeoVertPosStream as i32, read, write));
        let res = self.stream.res.borrow();
        Ok(slice::from_raw_parts_mut(ptr as *mut f32, res.num_verts() * 3))
        // ^ TODO remove multiplier when changing to proper type
    }

    unsafe fn raw_map_verts_tan(&self, read: bool, write: bool) -> Result<&[f32]> {
        let ptr = try!(self.stream.raw_map(ffi::H3DGeoRes::GeometryElem as i32, 0,
                                           ffi::H3DGeoRes::GeoVertTanStream as i32, read, write));
        let res = self.stream.res.borrow();
        Ok(slice::from_raw_parts_mut(ptr as *mut f32, res.num_verts() * 7))
        // ^ TODO remove multiplier when changing to proper type
    }

    unsafe fn raw_map_verts_attrib(&self, read: bool, write: bool) -> Result<&[f32]> {
        let ptr = try!(self.stream.raw_map(ffi::H3DGeoRes::GeometryElem as i32, 0,
                                           ffi::H3DGeoRes::GeoVertStaticStream as i32, read, write));
        let res = self.stream.res.borrow();
        Ok(slice::from_raw_parts_mut(ptr as *mut f32, res.num_verts() * 12))
        // ^ TODO remove multiplier when changing to proper type
    }
}





/////////////////////////////
// Texture-specific data
//

#[repr(i32)]
pub enum TexFormat {
    Unknown = 0,    // XXX https://github.com/rust-lang/rust/issues/24469
    BGRA8,
    DXT1,
    DXT3,
    DXT5,
    RGBA16F,
    RGBA32F
}

impl_res!(TexResData, TexResDataMut, TexRes, TexResMut, TexResStream, TexResFns,

    pub trait TexResFns : ResWrapper {

        /// Gets the number of subresources of the texture.
        ///
        /// A texture consists, depending on the type, of a number of equally sized slices which
        /// again can have a fixed number of mipmaps. Each image represents the base image
        /// of a slice or a single mipmap level of the corresponding slice.
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DTexRes::ImageElem`.
        fn num_images(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DTexRes::ImageElem as i32) as usize
            }
        }

        /// Gets the number of slices of the texture.
        ///
        /// This function returns 1 for 2D textures and 6 for cubemap.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DTexRes::TextureElem` with `H3DTexRes::TexSliceCountI`.
        fn num_slices(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DTexRes::TextureElem as i32,
                                     0, ffi::H3DTexRes::TexSliceCountI as i32) as usize
            }
        }

        /// Gets the width of a subresource of the texture.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DTexRes::ImageElem` with `H3DTexRes::ImgWidthI`.
        fn width(&self, image_id: usize) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DTexRes::ImageElem as i32,
                                     image_id as i32, ffi::H3DTexRes::ImgWidthI as i32) as usize
            }
        }

        /// Gets the height of a subresource of the texture.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DTexRes::ImageElem` with `H3DTexRes::ImgHeightI`.
        fn height(&self, image_id: usize) -> u32 {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DTexRes::ImageElem as i32,
                                     image_id as i32, ffi::H3DTexRes::ImgHeightI as i32) as u32
            }
        }

        /// Gets the texture pixel format.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DTexRes::TextureElem` with `H3DTexRes::TexFormatI`.
        fn format(&self) -> TexFormat {
            unsafe_h3d! {
                mem::transmute(
                    ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DTexRes::TextureElem as i32,
                                         0, ffi::H3DTexRes::TexFormatI as i32) as u32)
            }
        }
    }
);

/// Texture pixel data mapper.
pub struct TexResStream<'a, TRes: ResWrapper> {
    stream: UnsafeResStream<TRes>,
    phantom: PhantomData<&'a i32>,
}

impl<'a, TRes: TexRes> TexResStream<'a, TRes> {

    /// Constructs a texture mapper.
    ///
    /// Don't use this directly, use the `.as_stream()` method of texture accessors.
    ///
    pub fn new(res: TRes) -> TexResStream<'a, TRes> {
        TexResStream {
            stream: UnsafeResStream::new(res),
            phantom: PhantomData,
        }
    }

    /// Maps image pixels into a memory location for reading.
    ///
    /// # Panics
    ///
    /// Panics in case this mapper is already mapped.
    ///
    pub fn map_pixels(&self, image_id: usize) -> Result<*const u8> {
        let ptr = try!(unsafe { self.raw_map_pixels(image_id, true, false) });
        Ok(ptr as *const u8)
        // TODO maybe return something safer
    }

    /// Maps image pixels into a memory location for writing.
    ///
    /// In case of the need to read the mapped data, `allow_read` should be set to `true`
    /// otherwise the returned memory location may not contain meaningful data.
    ///
    /// # Panics
    ///
    /// Panics if the wrapped resource is immutable (`TRes` instead of `TResMut`).
    /// Also panics in case this mapper is already mapped.
    ///
    pub fn map_pixels_mut(&mut self, image_id: usize, allow_read: bool) -> Result<*mut u8> {
        unsafe { self.raw_map_pixels(image_id, allow_read, true) }
    }

    /// Unmaps a previosly mapped data on this stream.
    ///
    /// Not necessary to call explicitly, this is called when the stream is dropped.
    ///
    /// # Panics
    ///
    /// Panics if the stream is not mapped to anything.
    ///
    pub fn unmap(&self) {
        // Panics if not mapped
        unsafe { self.stream.raw_unmap(); }
    }

    unsafe fn raw_map_pixels(&self, image_id: usize, read: bool, write: bool) -> Result<*mut u8> {
        let ptr = try!(self.stream.raw_map(ffi::H3DTexRes::ImageElem as i32, image_id as i32,
                                           ffi::H3DTexRes::ImgPixelStream as i32, read, write));
        Ok(ptr)
    }
}







/////////////////////////////
// Animation-specific data
//

impl_res!(AnimResData, AnimResDataMut, AnimRes, AnimResMut, AnimResStream, AnimResFns,

    pub trait AnimResFns : ResWrapper {

        /// Gets the number of stored animation entities (joints and meshes).
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DAnimRes::EntityElem`.
        fn num_entities(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DAnimRes::EntityElem as i32) as usize
            }
        }

        /// Gets the number of frames stored for a specific animation entity.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DAnimRes::EntityElem` with `H3DAnimRes::EntFrameCountI`.
        fn num_frames(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DAnimRes::EntityElem as i32,
                                     0, ffi::H3DAnimRes::EntFrameCountI as i32) as usize
            }
        }
    }
);
impl_dummy_res_stream!(AnimResStream);





/////////////////////////////
// Material-specific data
//

impl_res!(MatResData, MatResDataMut, MatRes, MatResMut, MatResStream, MatResFns,

    pub trait MatResFns : ResWrapper {

        /// Gets the number of sampler elements in the material.
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DMatRes::SamplerElem`.
        fn num_samplers(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DMatRes::SamplerElem as i32) as usize
            }
        }

        /// Gets the number of uniform elements in the material.
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DMatRes::UniformElem`.
        fn num_uniforms(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DMatRes::UniformElem as i32) as usize
            }
        }

        /// Gets the class of the material.
        ///
        /// Wrapper for `h3dGetResParamStr` using `H3DMatRes::MaterialElem` with `H3DMatRes::MatClassStr`.
        fn class(&self) -> String {
            unsafe_h3d! {
                h3dGetResParamStr(self.as_res().raw(), ffi::H3DMatRes::MaterialElem as i32, 0,
                                  ffi::H3DMatRes::MatClassStr as i32)
            }
        }

        /// Sets the class of the material.
        ///
        /// Wrapper for `h3dSetResParamStr` using `H3DMatRes::MaterialElem` with `H3DMatRes::MatClassStr`.
        fn set_class(&mut self, class: &str) {
            unsafe_h3d! {
                h3dSetResParamStr(self.as_mut_res().raw(), ffi::H3DMatRes::MaterialElem as i32, 0,
                                  ffi::H3DMatRes::MatClassStr as i32, class)
            };
        }

        /// Gets the material resource that is linked to this material, like a parent.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DMatRes::MaterialElem` with `H3DMatRes::MatLinkI`.
        fn link(&self) -> Option<Res> {
            unsafe_h3d!({
                let handle = ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DMatRes::MaterialElem as i32,
                                                  0, ffi::H3DMatRes::MatLinkI as i32) as ffi::H3DRes;
                if handle == 0 { None } else { Some(Res::from_raw(handle)) }
            })
        }

        /// Sets the material resource that is linked to this material, like a parent.
        ///
        /// Wrapper for `h3dSetResParamI` using `H3DMatRes::MaterialElem` with `H3DMatRes::MatLinkI`.
        fn set_link(&mut self, mat: &TexRes) {
            unsafe_h3d! {
                ffi::h3dSetResParamI(self.as_mut_res().raw(), ffi::H3DMatRes::MaterialElem as i32,
                                     0, ffi::H3DMatRes::MatLinkI as i32, mat.as_res().raw())
            }
        }

        /// Gets the shader resource that is used by this material.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DMatRes::MaterialElem` with `H3DMatRes::MatShaderI`.
        fn shader(&self) -> Option<Res> {
            unsafe_h3d!({
                let handle = ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DMatRes::MaterialElem as i32,
                                                  0, ffi::H3DMatRes::MatShaderI as i32) as ffi::H3DRes;
                if handle == 0 { None } else { Some(Res::from_raw(handle)) }
            })
        }

        /// Gets the shader resource that is used by this material.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DMatRes::MaterialElem` with `H3DMatRes::MatShaderI`.
        fn set_shader(&mut self, shader: &ShaderRes) {
            unsafe_h3d! {
                ffi::h3dSetResParamI(self.as_mut_res().raw(), ffi::H3DMatRes::MaterialElem as i32,
                                     0, ffi::H3DMatRes::MatShaderI as i32, shader.as_res().raw())
            }
        }

        /// Gets the name of the sampler `sampler_id`.
        ///
        /// Wrapper for `h3dGetResParamStr` using `H3DMatRes::SamplerElem` with `H3DMatRes::SampNameStr`.
        fn sampler_name(&self, sampler_id: usize) -> String {
            unsafe_h3d! {
                h3dGetResParamStr(self.as_res().raw(), ffi::H3DMatRes::SamplerElem as i32,
                                  sampler_id as i32, ffi::H3DMatRes::SampNameStr as i32)
            }
        }

        /// Gets the texture resource bound to sampler `sampler_id`.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DMatRes::SamplerElem` with `H3DMatRes::SampTexResI`.
        fn sampler_tex(&self, sampler_id: usize) -> Result<Res> {
            unsafe_h3d!({
                let handle = ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DMatRes::SamplerElem as i32,
                                                  sampler_id as i32, ffi::H3DMatRes::SampTexResI as i32);
                if get_error_flag() { Err(get_error_string()) } else { Ok(Res::from_raw(handle)) }
            })
        }

        /// Sets the texture resource bound to sampler `sampler_id`.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DMatRes::SamplerElem` with `H3DMatRes::SampTexResI`.
        fn set_sampler_tex(&mut self, sampler_id: usize, tex: &TexRes) {
            unsafe_h3d! {
                ffi::h3dSetResParamI(self.as_mut_res().raw(), ffi::H3DMatRes::SamplerElem as i32,
                                     sampler_id as i32, ffi::H3DMatRes::SampTexResI as i32, tex.as_res().raw())
            }
        }

        /// Gets the name of the uniform `uniform_id`.
        ///
        /// Wrapper for `h3dGetResParamStr` using `H3DMatRes::UniformElem` with `H3DMatRes::UnifNameStr`.
        fn uniform_name(&self, uniform_id: usize) -> String {
            unsafe_h3d! {
                h3dGetResParamStr(self.as_res().raw(), ffi::H3DMatRes::UniformElem as i32,
                                  uniform_id as i32, ffi::H3DMatRes::UnifNameStr as i32)
            }
        }

        /// Gets the value of the uniform `uniform_id`.
        ///
        /// Wrapper for `h3dGetResParamF` using `H3DMatRes::UniformElem` with `H3DMatRes::UnifValueF4`.
        fn uniform(&self, uniform_id: usize) -> [f32; 4] {
            unsafe_h3d!({
                let mut abcd: [f32; 4] = { mem::uninitialized() };
                h3dGetResParamFN(self.as_res().raw(), ffi::H3DMatRes::UniformElem as i32, uniform_id as i32,
                                 ffi::H3DMatRes::UnifValueF4 as i32, 4, &mut abcd);
                abcd
            })
        }

        /// Sets the value of the uniform `uniform_id`.
        ///
        /// Wrapper for `h3dSetResParamF` using `H3DMatRes::UniformElem` with `H3DMatRes::UnifValueF4`.
        fn set_uniform(&mut self, uniform_id: usize, abcd: &[f32; 4]) {
            unsafe_h3d!({
                h3dSetResParamFN(self.as_mut_res().raw(), ffi::H3DMatRes::UniformElem as i32, uniform_id as i32,
                                 ffi::H3DMatRes::UnifValueF4 as i32, 4, abcd);
            })
        }

        /// Sets the value of the uniform `uniform_name`.
        ///
        /// Returns Err if the uniform was not found.
        ///
        /// Wrapper for `h3dSetMaterialUniform`.
        fn set_uniform_named(&mut self, uniform_name: &str, abcd: &[f32; 4]) -> Result<()> {
            with_c_str(uniform_name, |cname| {
                unsafe_h3d! {
                    if ffi::h3dSetMaterialUniform(self.as_mut_res().raw(), cname,
                                               abcd[0], abcd[1], abcd[3], abcd[4]) == 0 {
                        Err(format!("Uniform '{}' not found during h3dSetMaterialUniform call.", uniform_name))
                    } else {
                        Ok(())
                    }
                }
            })
        }
    }
);
impl_dummy_res_stream!(MatResStream);





/////////////////////////////
// Code-specific data
//

impl_res!(CodeResData, CodeResDataMut, CodeRes, CodeResMut, CodeResStream, CodeResFns,
    pub trait CodeResFns : ResWrapper {
        // Nothing to do here.
    }
);
impl_dummy_res_stream!(CodeResStream);





/////////////////////////////
// Shader-specific data
//

impl_res!(ShaderResData, ShaderResDataMut, ShaderRes, ShaderResMut, ShaderResStream, ShaderResFns,
    pub trait ShaderResFns : ResWrapper {

        /// Gets the number of context elements in the shader.
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DShaderRes::ContextElem`.
        fn num_contexts(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DShaderRes::ContextElem as i32) as usize
            }
        }

        /// Gets the number of sampler elements in the shader.
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DShaderRes::SamplerElem`.
        fn num_samplers(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DShaderRes::SamplerElem as i32) as usize
            }
        }

        /// Gets the number of uniform elements in the shader.
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DShaderRes::UniformElem`.
        fn num_uniforms(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DShaderRes::UniformElem as i32) as usize
            }
        }

        /// Gets the name of the context `context_id`.
        ///
        /// Wrapper for `h3dGetResParamStr` using `H3DShaderRes::ContextElem` with `H3DShaderRes::ContNameStr`.
        fn context_name(&self, context_id: usize) -> String {
            unsafe_h3d! {
                h3dGetResParamStr(self.as_res().raw(), ffi::H3DShaderRes::ContextElem as i32,
                                  context_id as i32, ffi::H3DShaderRes::ContNameStr as i32)
            }
        }

        /// Gets the name of the sampler `sampler_id`.
        ///
        /// Wrapper for `h3dGetResParamStr` using `H3DShaderRes::SamplerElem` with `H3DShaderRes::SampNameStr`.
        fn sampler_name(&self, sampler_id: usize) -> String {
            unsafe_h3d! {
                h3dGetResParamStr(self.as_res().raw(), ffi::H3DShaderRes::SamplerElem as i32,
                                  sampler_id as i32, ffi::H3DShaderRes::SampNameStr as i32)
            }
        }

        /// Gets the default texture resource of sampler.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DShaderRes::SamplerElem` with `H3DShaderRes::SampDefTexResI`.
        fn sampler_tex(&self, sampler_id: usize) -> Option<Res> {
            unsafe_h3d!({
                let handle = ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DShaderRes::SamplerElem as i32,
                                                  sampler_id as i32, ffi::H3DShaderRes::SampDefTexResI as i32) as ffi::H3DRes;
                if handle == 0 { None } else { Some(Res::from_raw(handle)) }
            })
        }

        /// Gets the name of the uniform `uniform_id`.
        ///
        /// Wrapper for `h3dGetResParamStr` using `H3DShaderRes::UniformElem` with `H3DShaderRes::UnifNameStr`.
        fn uniform_name(&self, uniform_id: usize) -> String {
            unsafe_h3d! {
                h3dGetResParamStr(self.as_res().raw(), ffi::H3DShaderRes::UniformElem as i32,
                                  uniform_id as i32, ffi::H3DShaderRes::UnifNameStr as i32)
            }
        }

        /// Gets the size (number of components) of the uniform `uniform_id`.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DShaderRes::UniformElem` with `H3DShaderRes::UnifSizeI`.
        fn uniform_size(&self, uniform_id: usize) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DShaderRes::UniformElem as i32,
                                     uniform_id as i32, ffi::H3DShaderRes::UnifSizeI as i32) as usize
            }
        }

        /// Gets the default value of the uniform `uniform_id`.
        ///
        /// Wrapper for `h3dGetResParamF` using `H3DShaderRes::UniformElem` with `H3DShaderRes::UnifDefValueF4`.
        fn uniform(&self, uniform_id: usize) -> [f32; 4] {
            unsafe_h3d!({
                let mut abcd: [f32; 4] = { mem::uninitialized() };
                h3dGetResParamFN(self.as_res().raw(), ffi::H3DShaderRes::UniformElem as i32, uniform_id as i32,
                                 ffi::H3DShaderRes::UnifDefValueF4 as i32, 4, &mut abcd);
                abcd
            })
        }

        /// Sets the default value of the uniform `uniform_id`.
        ///
        /// Wrapper for `h3dSetResParamF` using `H3DShaderRes::UniformElem` with `H3DShaderRes::UnifDefValueF4`.
        fn set_uniform(&mut self, uniform_id: usize, abcd: &[f32; 4]) {
            unsafe_h3d!({
                h3dSetResParamFN(self.as_mut_res().raw(), ffi::H3DShaderRes::UniformElem as i32, uniform_id as i32,
                                 ffi::H3DShaderRes::UnifDefValueF4 as i32, 4, abcd);
            })
        }
    }
);
impl_dummy_res_stream!(ShaderResStream);






/////////////////////////////
// Particle-specific data
//

#[repr(i32)]
pub enum ParticleChannel {
    MoveVelocity = 801, // XXX https://github.com/rust-lang/rust/issues/24469
    RotVelocity,
    Size,
    ColorR,
    ColorG,
    ColorB,
    ColorA,
    Drag = 813,         // XXX https://github.com/rust-lang/rust/issues/24469
}

unsafe fn get_particle_mm(res: i32, elem: i32, comp_min: i32, comp_max: i32) -> (f32, f32) {
    (ffi::h3dGetResParamF(res, elem, 0, comp_min, 0),
    ffi::h3dGetResParamF(res, elem, 0, comp_max, 0))
}

unsafe fn set_particle_mm(res: i32, elem: i32, comp_min: i32, comp_max: i32, mm: (f32, f32)) {
    ffi::h3dSetResParamF(res, elem, 0, comp_min, 0, mm.0);
    ffi::h3dSetResParamF(res, elem, 0, comp_max, 0, mm.1);
}

impl_res!(PartEffResData, PartEffResDataMut, PartEffRes, PartEffResMut, PartEffResStream, PartEffResFns,
    pub trait PartEffResFns : ResWrapper {

        /// Gets the `(min, max)` value of the particle random life time (in seconds).
        ///
        /// Wrapper for `h3dGetResParamF` using `H3DPartEffRes::ParticleElem` with
        /// `ffi::H3DPartEffRes::PartLifeMinF` and `ffi::H3DPartEffRes::PartLifeMaxF`.
        fn lifetime(&self) -> (f32, f32) {
            unsafe_h3d! {
                get_particle_mm(self.as_res().raw(), ffi::H3DPartEffRes::ParticleElem as i32,
                                ffi::H3DPartEffRes::PartLifeMinF as i32, ffi::H3DPartEffRes::PartLifeMaxF as i32)
            }
        }

        /// Sets the `(min, max)` value of the particle random life time (in seconds).
        ///
        /// Wrapper for `h3dSetResParamF` using `H3DPartEffRes::ParticleElem` with
        /// `ffi::H3DPartEffRes::PartLifeMinF` and `ffi::H3DPartEffRes::PartLifeMaxF`.
        fn set_lifetime(&mut self, minmax: (f32, f32)) {
            unsafe_h3d! {
                set_particle_mm(self.as_res().raw(), ffi::H3DPartEffRes::ParticleElem as i32,
                                ffi::H3DPartEffRes::PartLifeMinF as i32, ffi::H3DPartEffRes::PartLifeMaxF as i32,
                                minmax)
            };
        }

        /// Gets the `(min, max)` value range for selecting initial random value of a particle channel.
        ///
        /// Wrapper for `h3dGetResParamF` using `channel` with
        /// `ffi::H3DPartEffRes::ChanStartMinF` and `ffi::H3DPartEffRes::ChanStartMaxF`.
        fn channel_start(&self, channel: ParticleChannel) -> (f32, f32) {
            unsafe_h3d! {
                get_particle_mm(self.as_res().raw(), channel as i32,
                                ffi::H3DPartEffRes::ChanStartMinF as i32, ffi::H3DPartEffRes::ChanStartMaxF as i32)
            }
        }

        /// Sets the `(min, max)` value range for selecting initial random value of a particle channel.
        ///
        /// Wrapper for `h3dSetResParamF` using `channel` with
        /// `ffi::H3DPartEffRes::ChanStartMinF` and `ffi::H3DPartEffRes::ChanStartMaxF`.
        fn set_channel_start(&mut self, channel: ParticleChannel, minmax: (f32, f32)){
            unsafe_h3d! {
                set_particle_mm(self.as_mut_res().raw(), channel as i32,
                                ffi::H3DPartEffRes::ChanStartMinF as i32, ffi::H3DPartEffRes::ChanStartMaxF as i32,
                                minmax)
            }
        }

        /// Gets the remaining percentage of initial value when particle is dying.
        ///
        /// Wrapper for `h3dGetResParamF` using `channel` with `ffi::H3DPartEffRes::ChanEndRateF`.
        fn channel_end_rate(&self, channel: ParticleChannel) -> f32 {
            unsafe_h3d! {
                ffi::h3dGetResParamF(self.as_res().raw(), channel as i32, 0,
                                ffi::H3DPartEffRes::ChanEndRateF as i32, 0)
            }
        }

        /// Sets the remaining percentage of initial value when particle is dying.
        ///
        /// Wrapper for `h3dSetResParamF` using `channel` with `ffi::H3DPartEffRes::ChanEndRateF`.
        fn set_channel_end_rate(&mut self, channel: ParticleChannel, rate: f32) {
            unsafe_h3d! {
                ffi::h3dSetResParamF(self.as_mut_res().raw(), channel as i32, 0,
                                ffi::H3DPartEffRes::ChanEndRateF as i32, 0, rate)
            }
        }
    }
);
impl_dummy_res_stream!(PartEffResStream);






/////////////////////////////
// Pipeline-specific data
//

impl_res!(PipeResData, PipeResDataMut, PipeRes, PipeResMut, PipeResStream, PipeResFns,

    pub trait PipeResFns : ResWrapper {

        /// Gets the number of pipeline stages.
        ///
        /// Wrapper for `h3dGetResElemCount` using `H3DPipeRes::StageElem`.
        fn num_stages(&self) -> usize {
            unsafe_h3d! {
                ffi::h3dGetResElemCount(self.as_res().raw(), ffi::H3DPipeRes::StageElem as i32) as usize
            }
        }

        /// Gets the name of the the pipeline stage `stage_id`.
        ///
        /// Wrapper for `h3dGetResParamStr` using `H3DPipeRes::StageElem` with `H3DPipeRes::StageNameStr`.
        fn stage_name(&self, stage_id: usize) -> String {
            unsafe_h3d! {
                string_from_c_str(
                        ffi::h3dGetResParamStr(self.as_res().raw(), ffi::H3DPipeRes::StageElem as i32,
                                               stage_id as i32, ffi::H3DPipeRes::StageNameStr as i32)
                    )
            }
        }

        /// Gets whether the pipeline stage `stage_id` is active.
        ///
        /// Wrapper for `h3dGetResParamI` using `H3DPipeRes::StageElem` with `H3DPipeRes::StageActivationI`.
        fn is_stage_active(&self, stage_id: usize) -> bool {
            unsafe_h3d! {
                ffi::h3dGetResParamI(self.as_res().raw(), ffi::H3DPipeRes::StageElem as i32,
                                     stage_id as i32, ffi::H3DPipeRes::StageActivationI as i32) != 0
            }
        }

        /// Sets whether the pipeline stage `stage_id` is active.
        ///
        /// Wrapper for `h3dSetResParamI` using `H3DPipeRes::StageElem` with `H3DPipeRes::StageActivationI`.
        fn set_stage_active(&mut self, stage_id: usize, active: bool) {
            unsafe_h3d! {
                ffi::h3dSetResParamI(self.as_mut_res().raw(), ffi::H3DPipeRes::StageElem as i32,
                                     stage_id as i32, ffi::H3DPipeRes::StageActivationI as i32, active as i32)
            }
        }

        /// Changes the size of the render targets of a pipeline.
        ///
        /// This function sets the base width and height which affects render targets with
        /// relative (in percent) size specification.  Changing the base size is usually desired
        /// after engine initialization and when the window is being resized.
        ///
        /// Note that in case several cameras use the same pipeline resource instance, the change will affect all cameras.
        ///
        /// Wrapper for `h3dResizePipelineBuffers`.
        fn resize_pipeline_buffers(&mut self, width: usize, height: usize) {
            unsafe_h3d! {
                ffi::h3dResizePipelineBuffers(self.as_mut_res().raw(), width as i32, height as i32)
            }
        }

/* TODO
        /// Reads back the pixel data of a render target buffer.
        ///
        ///
        fn render_target_data(&self, )
*/
    }
);
impl_dummy_res_stream!(PipeResStream);





/////////////////////////////
// SceneGraph-specific data
//

impl_res!(SceneGraphResData, SceneGraphResDataMut, SceneGraphRes, SceneGraphResMut,
           SceneGraphResStream, SceneGraphResFns,
    pub trait SceneGraphResFns : ResWrapper {
        // Nothing to do here.
    }
);
impl_dummy_res_stream!(SceneGraphResStream);
