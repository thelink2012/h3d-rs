use std::ffi::{CString, CStr};
use libc::{c_char};
use std::sync::atomic::{AtomicBool, ATOMIC_BOOL_INIT};
use ffi;
use get_message;

/// Only one Horde3D engine instance may be initialized at once.
/// Set to false by default (not initialized).
///
/// See `h3d.rs` for it's set up.
pub static IS_H3D_ALIVE: AtomicBool = ATOMIC_BOOL_INIT;

/// Consumes the engine message queue getting all error level and debug level messages.
///
/// If no message is available on the queue returns `"Unknown error"`.
pub fn get_error_string() -> String {
    let mut result = String::new();
    while let Some((message, level, _)) = get_message() {
        if level == 1 || level == 4 {
            if !result.is_empty() { result.push('\n'); }
            result.push_str(&message);
        }
    }
    if result.is_empty() { result = "Unknown error".to_string(); }
    result
}

/// Checks the engine error flag then resets it.
///
/// This function shouldn't be exposed to the public, they should inspect Result<>.
///
/// Wrapper for `h3dGetError`.
pub fn get_error_flag() -> bool { // TODO Rename as get_error
    unsafe { ffi::h3dGetError() != 0 }
}

/// Checks whether the engine has been initialized with `h3d::init`.
#[inline(always)]
pub fn is_engine_alive() -> bool {
    use std::sync::atomic::Ordering;
    IS_H3D_ALIVE.load(Ordering::Relaxed)
}

/// Converts a null terminated C string into a Rust owned `String`.
pub unsafe fn string_from_c_str(c_str: *const c_char) -> String {
    String::from_utf8_lossy(CStr::from_ptr(c_str).to_bytes()).to_string()
}

/// Calls the functor `f` with the string slice `s` converted to a C string.
pub fn with_c_str<F, T>(s: &str, f: F) -> T where F: FnOnce(*const c_char) -> T {
    let c_str = CString::new(s.as_bytes());
    f(c_str.unwrap().as_bytes_with_nul().as_ptr() as *const _)
}


// TODO doc
// on doc notice that after this get_error_flag is reset

#[macro_export]
macro_rules! capsule_h3d {
    ($CodeBlock:expr) => {
        {
            // Makes sure the engine has been initialized.
            // perform this check only in debug builds, should be enough.
            {
                use $crate::util::is_engine_alive;
                debug_assert!(is_engine_alive(),
                              "Horde3D engine not initialized, please use `let engine = h3d::init()`.");
            }

            // Scope with access to Horde3D error flags and strings.
            // Checks if there's any error pending, if there is, we did something wrong back in another call.
            {
                use $crate::util::{get_error_flag, get_error_string};
                if get_error_flag() {
                    panic!("Unexpected unhandled error BEFORE Horde3D call: {}", get_error_string());
                }
            }

            //
            let hcapsule_macro_result_vvv99 = { $CodeBlock };

            // Check for errors again, this time for errors not handled $CodeBlock.
            {
                use $crate::util::{get_error_flag, get_error_string};
                if get_error_flag() {
                    panic!("Error on Horde3D call: {}", get_error_string());
                }
            }

            hcapsule_macro_result_vvv99 // fancy name
        }
    }
}

#[macro_export]
macro_rules! unsafe_h3d {
    ($CodeBlock:expr) => {
        unsafe { capsule_h3d!($CodeBlock) }
    }
}
