extern crate h3d;
mod suit;

/*
XXX Crashes Horde3D mmmmn
#[should_panic]
#[test]
fn test_invalid_load() {
    suit::run(|| {
        let _not_geo = h3d::Res::new_material("models/sphere/sphere.geo", h3d::ResFlags::empty()).unwrap();
        suit::load_resources();
    })
}
*/

#[test]
fn test_kind() {
    suit::run(|| {

        let mut no_query = h3d::Res::new_geometry("DoesNotExist", h3d::res::NO_QUERY).unwrap();
        let mut tex_jpg_res = h3d::Res::new_texture("textures/models/layingrock.jpg", h3d::ResFlags::empty(), h3d::TexResFlags::empty()).unwrap();
        let mut tex_dds_res = h3d::Res::new_texture("textures/ambientMap.dds", h3d::ResFlags::empty(), h3d::TexResFlags::empty()).unwrap();
        let mut tex_tga_res = h3d::Res::new_texture("textures/particles/particle1.tga", h3d::ResFlags::empty(), h3d::TexResFlags::empty()).unwrap();
        let mut geo_res  = h3d::Res::new_geometry("models/sphere/sphere.geo", h3d::ResFlags::empty()).unwrap();
        let mut anim_res = h3d::Res::new_animation("animations/man.anim", h3d::ResFlags::empty()).unwrap();
        let mut mat_res  = h3d::Res::new_material("materials/light.material.xml", h3d::ResFlags::empty()).unwrap();
        let mut pipe_res  = h3d::Res::new_pipeline("pipelines/forward.pipeline.xml", h3d::ResFlags::empty()).unwrap();
        let mut shader_res = h3d::Res::new_shader("shaders/skybox.shader", h3d::ResFlags::empty()).unwrap();
        let mut scene_res = h3d::Res::new_scene_graph("models/platform/platform.scene.xml", h3d::ResFlags::empty()).unwrap();
        let mut fx_res = h3d::Res::new_particle_effect("particles/particleSys1/particle1.particle.xml", h3d::ResFlags::empty()).unwrap();

        let mut test_kind = || {
            assert_eq!(no_query.kind(), h3d::ResType::Geometry);
            assert_eq!(tex_jpg_res.kind(), h3d::ResType::Texture);
            assert_eq!(tex_dds_res.kind(), h3d::ResType::Texture);
            assert_eq!(tex_tga_res.kind(), h3d::ResType::Texture);
            assert_eq!(geo_res.kind(), h3d::ResType::Geometry);
            assert_eq!(anim_res.kind(), h3d::ResType::Animation);
            assert_eq!(mat_res.kind(), h3d::ResType::Material);
            assert_eq!(pipe_res.kind(), h3d::ResType::Pipeline);
            assert_eq!(shader_res.kind(), h3d::ResType::Shader);
            assert_eq!(scene_res.kind(), h3d::ResType::SceneGraph);
            assert_eq!(fx_res.kind(), h3d::ResType::ParticleEffect);

            // Ensure 'as_' is working propery (won't panic if correct type).
            geo_res.as_geometry();
            geo_res.as_geometry_mut();
            no_query.as_geometry();
            no_query.as_geometry_mut();
            tex_jpg_res.as_texture();
            tex_jpg_res.as_texture_mut();
            tex_dds_res.as_texture();
            tex_dds_res.as_texture_mut();
            tex_tga_res.as_texture();
            tex_tga_res.as_texture_mut();
            anim_res.as_animation();
            anim_res.as_animation_mut();
            mat_res.as_material();
            mat_res.as_material_mut();
            pipe_res.as_pipeline();
            pipe_res.as_pipeline_mut();
            shader_res.as_shader();
            shader_res.as_shader_mut();
            scene_res.as_scene_graph();
            scene_res.as_scene_graph_mut();
            fx_res.as_particle_effect();
            fx_res.as_particle_effect_mut();
        };

        test_kind();
        suit::load_resources();
        test_kind();

        // TODO test Code resource
    })
}

#[should_panic]
#[test]
fn test_invalid_as_cast() {
    suit::run(|| {
        let pipe_res = h3d::Res::new_pipeline("Test", h3d::ResFlags::empty()).unwrap();
        pipe_res.as_geometry();
    })
}

#[test]
fn test_name() {
    suit::run(||{
        let res1 = h3d::Res::new_geometry("SameName", h3d::ResFlags::empty()).unwrap();
        let res2 = h3d::Res::new_material("SameName", h3d::ResFlags::empty()).unwrap();
        let rese = h3d::Res::new_geometry("SameName", h3d::ResFlags::empty());

        // Same name works for different resource types but not the other way around.
        assert!(rese.is_err());
        assert_eq!(res1.name(), "SameName");
        assert_eq!(res1.name(), res2.name());


        let res1f = h3d::Res::find(h3d::ResType::Geometry, "SameName").unwrap();
        let res2f = h3d::Res::find(h3d::ResType::Material, "SameName").unwrap();
        assert!(h3d::Res::find(h3d::ResType::Geometry, "DoesNotExist").is_none());
        // Check if resource handles are correct.
        unsafe {
            assert_eq!(res1f.raw(), res1.raw());
            assert_eq!(res2f.raw(), res2.raw());
            assert_eq!(res1f.name(), res1.name());
            assert_eq!(res2f.name(), res2.name());
            assert_eq!(res1f.name(), res2f.name());   // allowed
        }

        assert!(res1.clone_named("SameName").is_err());
        assert!(res1.clone_named("UnusedName").is_ok());
        assert!(!res1.clone_named("").unwrap().name().is_empty());    // anonymous name
    })
}

#[test]
fn test_load() {
    suit::run(||{

        let mat_string = r#"<Material>
                    	       <Shader source="shaders/deferredLighting.shader"/>
                         </Material>"#;

        let no_query = h3d::Res::new_geometry("WillNotLoad", h3d::res::NO_QUERY).unwrap();
        let mut mat_res  = h3d::Res::new_material("materials/light.material.xml", h3d::ResFlags::empty()).unwrap();

        assert!(!mat_res.is_loaded());
        assert!(!no_query.is_loaded());
        suit::load_resources();
        assert!(mat_res.is_loaded());
        assert!(!no_query.is_loaded());

        assert!(mat_res.is_loaded());
        assert!(mat_res.load(Some(mat_string.as_bytes())).is_err());
        assert!(mat_res.is_loaded());
        mat_res.unload();

        assert!(!mat_res.is_loaded());
        assert!(mat_res.load(Some(mat_string.as_bytes())).is_ok());
        assert!(mat_res.is_loaded());
        mat_res.unload();

        assert!(!mat_res.is_loaded());
        assert!(mat_res.load(None).is_ok());
        assert!(!mat_res.is_loaded());
    })
}

#[test]
fn test_consts() {
    extern crate h3d_sys as ffi;

    // Due to the fact we are setting constants manually instead of taking it from ffi in our code,
    // we must write a unit test to make sure things are correct.

    // Resource Types
    assert_eq!(h3d::ResType::Undefined as i32, ffi::H3DResTypes::Undefined as i32);
    assert_eq!(h3d::ResType::SceneGraph as i32, ffi::H3DResTypes::SceneGraph as i32);
    assert_eq!(h3d::ResType::Geometry as i32, ffi::H3DResTypes::Geometry as i32);
    assert_eq!(h3d::ResType::Animation as i32, ffi::H3DResTypes::Animation as i32);
    assert_eq!(h3d::ResType::Material as i32, ffi::H3DResTypes::Material as i32);
    assert_eq!(h3d::ResType::Code as i32, ffi::H3DResTypes::Code as i32);
    assert_eq!(h3d::ResType::Shader as i32, ffi::H3DResTypes::Shader as i32);
    assert_eq!(h3d::ResType::Texture as i32, ffi::H3DResTypes::Texture as i32);
    assert_eq!(h3d::ResType::ParticleEffect as i32, ffi::H3DResTypes::ParticleEffect as i32);
    assert_eq!(h3d::ResType::Pipeline as i32, ffi::H3DResTypes::Pipeline as i32);

    // Flags
    assert_eq!(h3d::res::NO_QUERY.bits(), ffi::H3DResFlags::NoQuery as i32);
    assert_eq!(h3d::res::NO_TEX_COMPRESSION.bits(), ffi::H3DResFlags::NoTexCompression as i32);
    assert_eq!(h3d::res::NO_TEX_MIPMAPS.bits(), ffi::H3DResFlags::NoTexMipmaps as i32);
    assert_eq!(h3d::res::TEX_CUBEMAP.bits(), ffi::H3DResFlags::TexCubemap as i32);
    assert_eq!(h3d::res::TEX_DYNAMIC.bits(), ffi::H3DResFlags::TexDynamic as i32);
    assert_eq!(h3d::res::TEX_RENDERABLE.bits(), ffi::H3DResFlags::TexRenderable as i32);
    assert_eq!(h3d::res::TEX_SRGB.bits(), ffi::H3DResFlags::TexSRGB as i32);

    // Tex Formats
    assert_eq!(h3d::TexFormat::Unknown as i32, ffi::H3DFormats::Unknown as i32);
    assert_eq!(h3d::TexFormat::BGRA8 as i32, ffi::H3DFormats::TEX_BGRA8 as i32);
    assert_eq!(h3d::TexFormat::DXT1 as i32, ffi::H3DFormats::TEX_DXT1 as i32);
    assert_eq!(h3d::TexFormat::DXT3 as i32, ffi::H3DFormats::TEX_DXT3 as i32);
    assert_eq!(h3d::TexFormat::DXT5 as i32, ffi::H3DFormats::TEX_DXT5 as i32);
    assert_eq!(h3d::TexFormat::RGBA16F as i32, ffi::H3DFormats::TEX_RGBA16F as i32);
    assert_eq!(h3d::TexFormat::RGBA32F as i32, ffi::H3DFormats::TEX_RGBA32F as i32);

    // Particle Channels
    assert_eq!(h3d::ParticleChannel::MoveVelocity as i32, ffi::H3DPartEffRes::ChanMoveVelElem as i32);
    assert_eq!(h3d::ParticleChannel::RotVelocity as i32, ffi::H3DPartEffRes::ChanRotVelElem as i32);
    assert_eq!(h3d::ParticleChannel::Size as i32, ffi::H3DPartEffRes::ChanSizeElem as i32);
    assert_eq!(h3d::ParticleChannel::ColorR as i32, ffi::H3DPartEffRes::ChanColRElem as i32);
    assert_eq!(h3d::ParticleChannel::ColorG as i32, ffi::H3DPartEffRes::ChanColGElem as i32);
    assert_eq!(h3d::ParticleChannel::ColorB as i32, ffi::H3DPartEffRes::ChanColBElem as i32);
    assert_eq!(h3d::ParticleChannel::ColorA as i32, ffi::H3DPartEffRes::ChanColAElem as i32);
    assert_eq!(h3d::ParticleChannel::Drag as i32, ffi::H3DPartEffRes::ChanDragElem as i32);
}
