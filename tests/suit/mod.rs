extern crate glfw;
extern crate h3d;
use std::sync::{Once, ONCE_INIT};
use std::sync::mpsc::{Receiver};
use self::glfw::{Context};

// Note: Run tests using the enviroment variable RUST_TEST_THREADS=1 for now.

static INIT_GLFW: Once = ONCE_INIT;
static mut GLFW_OBJ: Option<glfw::Glfw> = None;

struct TestSuit {
    _window: glfw::Window,
    _events: Receiver<(f64, glfw::WindowEvent)>,
    _engine: h3d::Engine,
}

impl TestSuit {

    pub fn new() -> TestSuit {

        INIT_GLFW.call_once(|| {
            let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();
            glfw.window_hint(glfw::WindowHint::Visible(false));
            unsafe { GLFW_OBJ = Some(glfw); }
        });

        //
        // Make another rendering context for testing.
        //

        let (mut window, events) = unsafe { GLFW_OBJ.unwrap() }
            .create_window(640, 480, "Testing Window", glfw::WindowMode::Windowed)
            .expect("Failed to create GLFW window.");

        window.set_size_polling(true);
        window.set_key_polling(true);
        window.make_current();

        TestSuit {
            _window: window,
            _events: events,
            _engine: h3d::init().unwrap(),
        }
    }
}

pub fn run<F>(mut test: F) where F: FnMut() {
    let _suit = TestSuit::new();
    test();
}

pub fn load_resources() {
    h3d::load_resources_from_disk("examples/assets").unwrap();
}
